import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './pages/layouts/admin-layout/admin-layout.component';
import { ForgotPasswordDialog, LoginComponent } from './pages/login/login.component';
import { MatDialogModule, MatFormFieldModule, MatInputModule, MatSnackBarModule } from '@angular/material';
import { RegisterComponent } from './pages/register/register.component';
import { AuthGuard } from './domains/auth.guard';
import { SwPush, ServiceWorkerModule } from '@angular/service-worker';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';
import { TermsConditionsComponent } from './pages/terms-conditions/terms-conditions.component';

@NgModule({
  imports: [
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: true }),
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ComponentsModule,
    RouterModule,
    MatInputModule,
    MatFormFieldModule,
    AppRoutingModule,
    MatSnackBarModule,
    MatDialogModule,
    // AgmCoreModule.forRoot({
    //   apiKey: 'YOUR_GOOGLE_MAPS_API_KEY'
    // })
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    LoginComponent,
    RegisterComponent,
    ResetPasswordComponent,
    ForgotPasswordDialog,
    PrivacyPolicyComponent,
    TermsConditionsComponent
  ],
  providers: [
    AuthGuard,
    SwPush
  ],
  entryComponents: [ForgotPasswordDialog],
  bootstrap: [AppComponent]
})
export class AppModule { }
