import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'app/domains/models/user';
import { SynergeService } from 'app/services/synerge.service';
import { ApiService } from 'app/services/api.service';
import { LoggerService } from 'app/services/logger.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { ValidatorService } from 'app/services/validator.service';
import { ResponseFormat } from 'app/domains/response-format';
import { MatDialog } from '@angular/material';
import { ChangePasswordDialogComponent } from 'app/components/change-password-dialog/change-password-dialog.component';
import { AppConstants } from 'app/domains/app-constants';
import { Clients } from 'app/domains/models/clients';
import { ImageCropperDialogComponent } from 'app/components/image-cropper-dialog/image-cropper-dialog.component';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {
  editProfileForm: FormGroup;
  user: User;
  clients: Clients[] = [];
  companyNames = [];

  imagePath;
  imgURL: any;

  buttonText = "Save";
  message: string;

  showChangePassword = true;
  isAdmin = localStorage.getItem(AppConstants.USER_ROLE) === 'ADMIN' ? true : false;

  constructor(private location: Location,
    public synergeService: SynergeService,
    private apiService: ApiService,
    private logger: LoggerService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      let userId = params['user_id'];
      if (userId) {
        if (this.isAdmin) {
          this.apiService.getAllActiveClients().subscribe((res: ResponseFormat) => {
            this.logger.debug("Got clients count: " + res.data.length);
            this.clients = res.data;
            this.getUserData(userId);
          }, error => {
            this.synergeService.handleErrorOnAPI(error);
          });
        } else {
          this.getUserData(userId);
        }
      } else {
        this.synergeService.handleError('Invalid params. `user_id` is empty');
      }
    });
  }

  getUserData(userId) {
    this.apiService.getUserById(userId).subscribe(data => {
      this.logger.debug("Edit Profile - User by ID: " + JSON.stringify(data));
      this.user = data.data;
      this.onBelongsToChange(this.user.belongs_to);
      if (this.user._id !== localStorage.getItem(AppConstants.USER_ID)) {
        this.showChangePassword = false;
      }
      this.editProfileForm = this.formBuilder.group({
        first_name: new FormControl(this.user.first_name, Validators.required),
        last_name: new FormControl(this.user.last_name, Validators.required),
        user_name: new FormControl(this.user.user_name, Validators.required),
        email: new FormControl(this.user.email, [Validators.required, Validators.email]),
        phone: new FormControl(this.user.phone, [Validators.required, ValidatorService.phoneMinLength]),
        role: new FormControl(this.user.role, Validators.required),
        belongs_to: new FormControl(this.user.belongs_to),
        company_name: new FormControl(this.user.company_name),
        address: new FormControl(this.user.address),
        city: new FormControl(this.user.city),
        country: new FormControl(this.user.country),
        postal_code: new FormControl(this.user.postal_code),
        designation: new FormControl(this.user.designation),
        bio: new FormControl(this.user.bio)
      });
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.editProfileForm.controls[controlName].hasError(errorName);
  }

  onBelongsToChange(belongsTo) {
    this.logger.debug("onBelongsToChange: " + belongsTo);
    if (belongsTo === 'SYNERGE_I') {
      this.companyNames = this.clients.filter(x => x.belongs_to === 'SYNERGE_I');
    } else {
      this.companyNames = this.clients.filter(x => x.belongs_to === 'SYNERGE_II');
    }
  }

  goBack() {
    this.location.back();
  }

  onSubmit() {
    if (this.editProfileForm.valid) {
      if (this.imagePath) {
        this.buttonText = "Loading...";
        let formData: FormData = new FormData();
        formData.append('uploadImage', this.imagePath);
        this.apiService.uploadImage(formData).subscribe((res: ResponseFormat) => {
          if (res.success) {
            this.user.profile_pic_path = '/uploads/' + res.data;
            this.updateUser();
          }
        }, error => {
          this.buttonText = "Save";
          this.synergeService.handleErrorOnAPI(error);
        });
      } else {
        this.updateUser();
      }
    }
  }

  updateUser() {
    this.user.first_name = this.editProfileForm.value.first_name;
    this.user.last_name = this.editProfileForm.value.last_name;
    this.user.user_name = this.editProfileForm.value.user_name;
    this.user.email = this.editProfileForm.value.email;
    this.user.phone = this.editProfileForm.value.phone;
    this.user.role = this.editProfileForm.value.role;
    this.user.belongs_to = this.editProfileForm.value.belongs_to;
    this.user.company_name = this.editProfileForm.value.company_name;
    this.user.address = this.editProfileForm.value.address;
    this.user.city = this.editProfileForm.value.city;
    this.user.country = this.editProfileForm.value.country;
    this.user.postal_code = this.editProfileForm.value.postal_code;

    this.buttonText = "Loading...";
    this.apiService.updateUser(this.user).subscribe((res: ResponseFormat) => {
      this.logger.debug("Update Profile - User by ID: " + JSON.stringify(res));
      if (res.success) {
        this.buttonText = "Success";
        setTimeout(() => {
          this.goBack();
        }, 1000);
      } else {
        this.buttonText = "Save";
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.buttonText = "Save";
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  preview(event) {
    if (event.target.files.length === 0)
      return;

    let mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    } else {
      this.message = "";
    }

    const eventDialog = this.dialog.open(ImageCropperDialogComponent, {
      width: '800px',
      data: event
    });
    eventDialog.afterClosed().subscribe(result => {
      if (result) {
        this.imgURL = result.croppedImage;
        this.imagePath = result.croppedFile;
      }
      const element = event.target as HTMLInputElement;
      element.value = '';
    });
  }

  onChangePassword() {
    const changePasswordDialog = this.dialog.open(ChangePasswordDialogComponent, {
      width: '600px',
      data: this.user
    });

    changePasswordDialog.afterClosed().subscribe(result => {
      if (result) {
        this.user = result;
      }
    });
  }
}