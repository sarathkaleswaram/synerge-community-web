import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/services/api.service';
import { Router } from '@angular/router';
import { ResponseFormat } from 'app/domains/response-format';
import { AppConstants } from 'app/domains/app-constants';
import { LoggerService } from 'app/services/logger.service';
import * as md5 from 'md5';
import { SynergeService } from 'app/services/synerge.service';
import { NotificationService } from 'app/services/notification.service';
import { MatDialog, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  body = {
    user_name: "",
    password: ""
  }
  buttonText = "Let`s go";
  message = "";

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private apiService: ApiService,
    private synergeService: SynergeService,
    private notificationService: NotificationService,
    private logger: LoggerService) { }

  ngOnInit() {
    this.synergeService.logout();
  }

  onSubmit() {
    this.message = "";
    if (this.body.user_name == "" || this.body.password == "") {
      this.message = "Please enter all fields.";
      return;
    }
    this.buttonText = "Loading...";
    this.apiService.loginUser({ user_name: this.body.user_name, password: md5(this.body.password) }).subscribe((res: ResponseFormat) => {
      if (res.success) {
        this.buttonText = "Success";
        this.logger.debug("Login routes: " + JSON.stringify(res.data.routes));
        localStorage.setItem(AppConstants.ROUTES, JSON.stringify(res.data.routes));
        localStorage.setItem(AppConstants.TOKEN, res.token);
        localStorage.setItem(AppConstants.USER_FULL_NAME, res.data.user.first_name + " " + res.data.user.last_name);
        localStorage.setItem(AppConstants.USER_ID, res.data.user._id);
        localStorage.setItem(AppConstants.USER_ROLE, res.data.user.role);
        localStorage.setItem(AppConstants.USER_BELONGS_TO, res.data.user.belongs_to);
        localStorage.setItem(AppConstants.USER_COMPANY_NAME, res.data.user.company_name);
        this.router.navigate(["/news-feed"]);
        setTimeout(() => {
          this.notificationService.openBetaVersionSnackBar();
        }, 1000);
      } else {
        this.buttonText = "Let`s go";
        this.message = res.message;
      }
    }, error => {
      this.buttonText = "Let`s go";
      this.message = error.message;
    });
  }

  onForgotPassword() {
    const dialogRef = this.dialog.open(ForgotPasswordDialog, {
      width: '450px',
    });
  }

}

@Component({
  selector: 'forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordDialog {
  email: string;
  message: string;
  buttonText = "Submit";

  constructor(
    private apiService: ApiService,
    public dialogRef: MatDialogRef<ForgotPasswordDialog>
  ) { }

  onClose(): void {
    this.dialogRef.close();
  }

  onSubmit() {
    this.message = "";
    if (this.email == "") {
      this.message = "Please enter email.";
      return;
    }
    this.buttonText = "Loading...";
    this.apiService.forgotPassword({ email: this.email }).subscribe((res: ResponseFormat) => {
      if (res.success) {
        this.buttonText = res.message;
        // this.onClose();
      } else {
        this.buttonText = "Submit";
        this.message = res.message;
      }
    }, error => {
      this.buttonText = "Submit";
      this.message = error.message;
    });
  }

}