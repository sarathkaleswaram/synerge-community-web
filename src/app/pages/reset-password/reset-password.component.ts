import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/services/api.service';
import { ResponseFormat } from 'app/domains/response-format';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ValidatorService } from 'app/services/validator.service';
import * as md5 from 'md5';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  resetPasswordForm: FormGroup;
  token: string;
  buttonText = "Submit";
  message = "";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private apiService: ApiService
  ) {
    this.route.queryParams.subscribe(params => {
      this.token = params['token'];
      if (!this.token) 
      this.message = "Missing token in params";
  });
  }

  ngOnInit() {
    this.resetPasswordForm = new FormGroup({
      newPassword: new FormControl('', [Validators.required, Validators.minLength(6)]),
      confirmPassword: new FormControl('', [Validators.required, ValidatorService.validateNewPassword])
    });
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.resetPasswordForm.controls[controlName].hasError(errorName);
  }

  onSubmit() {
    this.message = "";
    if (this.resetPasswordForm.valid) {
      this.buttonText = "Loading...";
      let user = {
        password: md5(this.resetPasswordForm.get('confirmPassword').value),
        token: this.token
      }
      this.apiService.resetPassword(user).subscribe((res: ResponseFormat) => {
        if (res.success) {
          this.buttonText = "Password reset successful";
          setTimeout(() => {
            this.router.navigate(["/login"]);
          }, 2000);
        } else {
          this.buttonText = "Submit";
          this.message = res.message;
        }
      }, error => {
        this.buttonText = "Submit";
        this.message = error.message;
      });
    }
  }

}
