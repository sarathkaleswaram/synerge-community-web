import { Component, OnInit, Inject } from '@angular/core';
import { Feedback, Comments } from 'app/domains/models/feedback';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SynergeService } from 'app/services/synerge.service';
import { ApiService } from 'app/services/api.service';
import { LoggerService } from 'app/services/logger.service';
import { ResponseFormat, SocketEventFormat } from 'app/domains/response-format';
import { AppConstants } from 'app/domains/app-constants';
import { AddFeedbackDialogComponent } from 'app/components/add-feedback-dialog/add-feedback-dialog.component';
import { ConfirmationDialogComponent } from 'app/components/confirmation-dialog/confirmation-dialog.component';
import { EditFeedbackDialogComponent } from 'app/components/edit-feedback-dialog/edit-feedback-dialog.component';
import { SocketService } from 'app/services/socket.service';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {
  feedbacks: Feedback[];
  isAdmin = localStorage.getItem(AppConstants.USER_ROLE) === 'ADMIN' ? true : false;

  deleteButton: boolean[] = [];

  constructor(public dialog: MatDialog,
    public synergeService: SynergeService,
    private apiService: ApiService,
    private socketService: SocketService,
    private logger: LoggerService) { }

  ngOnInit() {
    this.socketService.resetDotNotification();
    this.getFeedbacks();
    this.socketService.socketObservable.subscribe((data: SocketEventFormat) => {
      if (data.type === AppConstants.TYPE_FEEDBACK) {
        this.logger.debug("Socket Feedback component update");
        this.getFeedbacks();        
      }
    });
  }

  getFeedbacks() {
    this.apiService.getAllFeedbacks().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got Feedback count: " + res.data.length);
      if (res.success) {
        this.feedbacks = res.data;
        this.feedbacks.forEach((feedback, i) => {
          this.deleteButton[i] = false;
        });
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  onDelete(feedback: Feedback, i: number) {
    this.logger.debug("On Delete: " + JSON.stringify(feedback));
    const confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      width: '600px',
      data: `Do you want to delete ${feedback.feedback}?`
    });
    confirmationDialog.afterClosed().subscribe(result => {
      if (result) {
        this.deleteButton[i] = true;
        this.apiService.deleteFeedbackById(feedback._id).subscribe((res: ResponseFormat) => {
          this.logger.debug("Delete Feedback: " + JSON.stringify(res));
          if (res.success) {
            setTimeout(() => {
              this.getFeedbacks();
            }, 1000);
          } else {
            this.synergeService.handleError(res.message);
          }
        }, error => {
          this.synergeService.handleErrorOnAPI(error);
        });
      }
    });
  }

  onAdd() {
    let data = {
      belongs_to: localStorage.getItem(AppConstants.USER_BELONGS_TO),
      company_name: localStorage.getItem(AppConstants.USER_COMPANY_NAME)
    }
    const bookingDialog = this.dialog.open(AddFeedbackDialogComponent, {
      width: '800px',
      data: data
    });
    bookingDialog.afterClosed().subscribe(result => {
      if (result)
        this.getFeedbacks();
    });
  }

  editFeedback(feedback: Feedback) {    
    const feedbackDialog = this.dialog.open(EditFeedbackDialogComponent, {
      width: '800px',
      data: feedback._id
    });
    feedbackDialog.afterClosed().subscribe(result => {
      if (result && result === "Success") {
        this.getFeedbacks();
      }
    });
  }

  commentFeedback(feedback: Feedback) {
    this.dialog.open(FeedbackCommentDialogComponent, {
      data: feedback
    });
  }
}


@Component({
  selector: 'feedback-comments-dialog',
  templateUrl: './feedback-comments-dialog.html'
})
export class FeedbackCommentDialogComponent {
  newComment = "";

  constructor(public dialogRef: MatDialogRef<FeedbackCommentDialogComponent>,
    public synergeService: SynergeService,
    private apiService: ApiService,
    private logger: LoggerService,
    @Inject(MAT_DIALOG_DATA) public feedback: Feedback) { }

  getFeedback(feedback: Feedback) {
    this.apiService.getFeedbackById(feedback._id).subscribe((res: ResponseFormat) => {
      if (res.success) {
        this.feedback = res.data;
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  onSend() {
    let comment: Comments = {
      comment: this.newComment,
      by_user_id: localStorage.getItem(AppConstants.USER_ID),
      by_user_name: localStorage.getItem(AppConstants.USER_FULL_NAME),
      date: new Date()
    };
    this.newComment = "";
    this.feedback.comments.push(comment);
    this.apiService.updateFeedbackById(this.feedback).subscribe((res: ResponseFormat) => {
      this.logger.debug("Update feedback result: " + JSON.stringify(res));
      if (res.success) {
        this.getFeedback(this.feedback);
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }
}