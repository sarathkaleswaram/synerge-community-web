import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/services/api.service';
import { AppConstants } from 'app/domains/app-constants';
import { LoggerService } from 'app/services/logger.service';
import { SynergeService } from 'app/services/synerge.service';
import { User } from 'app/domains/models/user';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {
  user: User;

  constructor(private apiService: ApiService,
    public synergeService: SynergeService,
    private router: Router,
    private route: ActivatedRoute,
    private logger: LoggerService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      let userId = params['user_id'];
      if (userId) {
        this.apiService.getUserById(userId).subscribe(data => {
          this.logger.debug("User Info - User by ID: " + JSON.stringify(data));
          this.user = data.data;
        }, error => {
          this.synergeService.handleErrorOnAPI(error);
        });
      } else {
        this.synergeService.handleError('Invalid params. `user_id` is empty');
      }
    });
  }

}
