import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { LoggerService } from 'app/services/logger.service';
import { SynergeService } from 'app/services/synerge.service';
import { ApiService } from 'app/services/api.service';
import { Event, Likes, Comments } from 'app/domains/models/events';
import { ResponseFormat, SocketEventFormat } from 'app/domains/response-format';
import { AppConstants } from 'app/domains/app-constants';
import { EventDialogComponent } from 'app/components/event-dialog/event-dialog.component';
import { ConfirmationDialogComponent } from 'app/components/confirmation-dialog/confirmation-dialog.component';
import { SocketService } from 'app/services/socket.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {
  upcomingEvents: Event[];
  completedEvents: Event[];
  isAdmin = localStorage.getItem(AppConstants.USER_ROLE) === 'ADMIN' ? true : false;

  constructor(public dialog: MatDialog,
    public synergeService: SynergeService,
    private socketService: SocketService,
    private apiService: ApiService,
    private logger: LoggerService) { }

  ngOnInit() {
    this.socketService.resetDotNotification();
    this.getAllEvent();
    this.socketService.socketObservable.subscribe((data: SocketEventFormat) => {
      if (data.type === AppConstants.TYPE_EVENT) {
        this.logger.debug("Socket Event component update");
        this.getAllEvent();
      }
    });
  }

  getAllEvent() {
    this.apiService.getAllEvents().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got Total Events count: " + res.data.length);
      if (res.success) {
        let totalEvents: Event[] = res.data;
        this.upcomingEvents = totalEvents.filter(x => x.type === "Upcoming");
        this.logger.debug("Upcoming Events count: " + this.upcomingEvents.length);
        this.completedEvents = totalEvents.filter(x => x.type === "Completed");
        this.logger.debug("Completed Events count: " + this.completedEvents.length);
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  getDidILike(event: Event) {
    return event.likes.find(x => x.by_user_id === localStorage.getItem(AppConstants.USER_ID));
  }

  onAdd() {
    const eventDialog = this.dialog.open(EventDialogComponent, {
      width: '600px'
    });
    eventDialog.afterClosed().subscribe(result => {
      if (result && result === "Success") {
        this.getAllEvent();
      }
    });
  }

  editEvent(event: Event) {
    const eventDialog = this.dialog.open(EventDialogComponent, {
      width: '600px',
      data: event._id
    });
    eventDialog.afterClosed().subscribe(result => {
      if (result && result === "Success") {
        this.getAllEvent();
      }
    });
  }

  deleteEvent(event: Event) {
    const confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      width: '600px',
      data: `Do you want to delete ${event.title}?`
    });
    confirmationDialog.afterClosed().subscribe(result => {
      if (result) {
        this.apiService.deleteEventById(event._id).subscribe((res: ResponseFormat) => {
          this.logger.debug("Event Deleted by ID: " + JSON.stringify(res));
          if (res.success) {
            setTimeout(() => {
              this.getAllEvent();
            }, 1000);
          } else {
            this.synergeService.handleError(res.message);
          }
        }, error => {
          this.synergeService.handleErrorOnAPI(error);
        });
      }
    });
  }

  onLike(event: Event) {
    if (this.getDidILike(event)) {
      event.likes.splice(event.likes.findIndex(x => x.by_user_id === localStorage.getItem(AppConstants.USER_ID)), 1);
    } else {
      let like: Likes = {
        by_user_id: localStorage.getItem(AppConstants.USER_ID),
        by_user_name: localStorage.getItem(AppConstants.USER_FULL_NAME),
        date: new Date()
      };
      event.likes.push(like);
    }
    this.apiService.updateEventById(event).subscribe((res: ResponseFormat) => {
      this.logger.debug("Update events result: " + JSON.stringify(res));
      if (res.success) {
        this.getAllEvent();
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  onLikeList(event: Event) {
    this.dialog.open(EventLikesDialogComponent, {
      data: event.likes
    });
  }

  onComment(event: Event) {
    this.dialog.open(EventCommentDialogComponent, {
      data: event
    });
  }

}

@Component({
  selector: 'events-likes-dialog',
  templateUrl: './events-likes-dialog.html'
})
export class EventLikesDialogComponent {
  constructor(
    public synergeService: SynergeService,
    @Inject(MAT_DIALOG_DATA) public likes: Likes[],
    public dialogRef: MatDialogRef<EventLikesDialogComponent>) { }
}

@Component({
  selector: 'events-comments-dialog',
  templateUrl: './events-comments-dialog.html'
})
export class EventCommentDialogComponent {
  newComment = "";

  constructor(public dialogRef: MatDialogRef<EventCommentDialogComponent>,
    public synergeService: SynergeService,
    private apiService: ApiService,
    private logger: LoggerService,
    @Inject(MAT_DIALOG_DATA) public event: Event) { }

  getEvent(event: Event) {
    this.apiService.getEventById(event._id).subscribe((res: ResponseFormat) => {
      if (res.success) {
        this.event = res.data;
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  onSend() {
    let comment: Comments = {
      comment: this.newComment,
      by_user_id: localStorage.getItem(AppConstants.USER_ID),
      by_user_name: localStorage.getItem(AppConstants.USER_FULL_NAME),
      date: new Date()
    };
    this.newComment = "";
    this.event.comments.push(comment);
    this.apiService.updateEventById(this.event).subscribe((res: ResponseFormat) => {
      this.logger.debug("Update events result: " + JSON.stringify(res));
      if (res.success) {
        this.getEvent(this.event);
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }
}