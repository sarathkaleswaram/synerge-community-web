import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Booking, StatusEnum } from 'app/domains/models/bookings';
import { SynergeService } from 'app/services/synerge.service';
import { ApiService } from 'app/services/api.service';
import { LoggerService } from 'app/services/logger.service';
import { ResponseFormat, SocketEventFormat } from 'app/domains/response-format';
import { User } from 'app/domains/models/user';
import { AddBookingDialogComponent } from 'app/components/add-booking-dialog/add-booking-dialog.component';
import { BelongsToEnum, Clients } from 'app/domains/models/clients';
import { SocketService } from 'app/services/socket.service';
import { ActivatedRoute } from '@angular/router';
import { AppConstants } from 'app/domains/app-constants';

@Component({
  selector: 'app-all-bookings',
  templateUrl: './all-bookings.component.html',
  styleUrls: ['./all-bookings.component.scss']
})
export class AllBookingsComponent implements OnInit {
  bookings: Booking[];

  constructor(public dialog: MatDialog,
    public synergeService: SynergeService,
    private socketService: SocketService,
    private apiService: ApiService,
    private route: ActivatedRoute,
    private logger: LoggerService) { }

  ngOnInit() {
    this.socketService.resetDotNotification();
    this.route.queryParams.subscribe(params => {
      let bookingId = params['bookingId'];
      if (bookingId) {
        this.apiService.getBookingById(bookingId).subscribe((res: ResponseFormat) => {
          this.logger.debug("Got Booking by id: " + JSON.stringify(res));
          if (res.data) {
            this.onBookingSelect(res.data);
          } else {
            this.synergeService.handleError(res.message);
          }
        }, error => {
          this.synergeService.handleErrorOnAPI(error);
        });
      }
    });
    this.getBookings();
    this.socketService.socketObservable.subscribe((data: SocketEventFormat) => {
      if (data.type === AppConstants.TYPE_BOOKING) {
        this.logger.debug("Socket Bookings component update");
        this.getBookings();        
      }
    });
  }

  getBookings() {
    this.apiService.getAllBookings().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got Bookings count: " + res.data.length);
      if (res.success) {
        this.bookings = res.data;
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  onBookingSelect(booking: Booking) {
    const dialogRef = this.dialog.open(EditBookingDialogComponent, { width: '800px', data: booking });

    dialogRef.afterClosed().subscribe(result => {
      this.getBookings();
    });
  }

  onAdd() {
    const addAdminBookingDialog = this.dialog.open(AddAdminBookingDialogComponent, {
      width: '600px'
    });
    addAdminBookingDialog.afterClosed().subscribe(result => {
      if (result) {
        const bookingDialog = this.dialog.open(AddBookingDialogComponent, {
          width: '800px',
          data: result
        });
        bookingDialog.afterClosed().subscribe(result => {
          this.getBookings();
        });
      }
    });
  }
}

@Component({
  selector: 'add-admin-booking-dialog',
  templateUrl: './add-admin-booking-dialog.html'
})
export class AddAdminBookingDialogComponent {
  clients: Clients[] = [];
  companyNames = [];
  message = "";
  belongs_to: BelongsToEnum;
  company_name = "";

  constructor(public synergeService: SynergeService,
    private apiService: ApiService,
    private logger: LoggerService,
    public dialogRef: MatDialogRef<AddAdminBookingDialogComponent>) {
    this.apiService.getAllActiveClients().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got clients count: " + res.data.length);
      this.clients = res.data;
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  onBelongsToChange(belongsTo) {
    this.logger.debug("onBelongsToChange: " + belongsTo);
    if (belongsTo === 'SYNERGE_I') {
      this.companyNames = this.clients.filter(x => x.belongs_to === 'SYNERGE_I');
    } else {
      this.companyNames = this.clients.filter(x => x.belongs_to === 'SYNERGE_II');
    }
  }

  onSelect() {
    this.message = "";
    if (this.belongs_to === undefined || this.company_name === "") {
      this.message = "Please enter all fields.";
      return;
    }
    this.dialogRef.close({ belongs_to: this.belongs_to, company_name: this.company_name });
  }

  closeDialog() {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'edit-booking-dialog',
  templateUrl: './edit-booking-dialog.html'
})
export class EditBookingDialogComponent implements OnInit {
  user: User;
  status: StatusEnum;
  saveButtonText = "Save";
  deleteButtonText = "Delete";

  constructor(public dialogRef: MatDialogRef<EditBookingDialogComponent>,
    public synergeService: SynergeService,
    private apiService: ApiService,
    private logger: LoggerService,
    @Inject(MAT_DIALOG_DATA) public booking: Booking) {
    this.status = booking.status;
  }

  ngOnInit() {
    this.apiService.getUserById(this.booking.by_user_id).subscribe((res: ResponseFormat) => {
      this.logger.debug("Booked User: " + JSON.stringify(res));
      if (res.success) {
        this.user = res.data;
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  onUpdate() {
    this.saveButtonText = "Loading...";
    this.booking.status = this.status;
    this.apiService.updateBookingById(this.booking).subscribe((res: ResponseFormat) => {
      this.logger.debug("Update Booking: " + JSON.stringify(res));
      this.closeDialog();
      if (!res.success) {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  onDelete() {
    this.deleteButtonText = "Loading...";
    this.apiService.deleteBookingById(this.booking._id).subscribe((res: ResponseFormat) => {
      this.logger.debug("Delete Booking: " + JSON.stringify(res));
      this.closeDialog();
      if (!res.success) {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
