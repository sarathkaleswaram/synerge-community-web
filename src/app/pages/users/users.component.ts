import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'app/services/api.service';
import { LoggerService } from 'app/services/logger.service';
import { SynergeService } from 'app/services/synerge.service';
import { User } from 'app/domains/models/user';
import { ResponseFormat } from 'app/domains/response-format';
import { AppConstants } from 'app/domains/app-constants';
import { MatDialogRef, MatDialog } from '@angular/material';
import { ConfirmationDialogComponent } from 'app/components/confirmation-dialog/confirmation-dialog.component';
import { Clients } from 'app/domains/models/clients';
import { SocketService } from 'app/services/socket.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users = [];
  activateButtonText: string[] = [];
  deleteButtonText: string[] = [];
  roleAdmin = AppConstants.ROLE_ADMIN;

  constructor(private apiService: ApiService,
    private socketService: SocketService,
    private synergeService: SynergeService,
    private router: Router,
    public dialog: MatDialog,
    private logger: LoggerService) { }

  ngOnInit() {
    this.getUsersList();
  }

  getUsersList() {
    this.socketService.resetDotNotification();
    this.apiService.getAllUsers().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got users count: " + res.data.length);
      this.users = res.data;
      this.users.forEach((user: User, i: number) => {
        user.activated ? this.activateButtonText[i] = 'Inactivate' : this.activateButtonText[i] = 'Activate';
        this.deleteButtonText[i] = 'Delete';
      });
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  onUserClick(user: User) {
    this.router.navigate(["/edit-profile"], { queryParams: { user_id: user._id } });
  }

  onActivate(user: User, i: number) {
    this.logger.debug("On Activate: " + JSON.stringify(user));
    if (user.belongs_to) {
      if (user.activated) {
        user.activated = false;
      } else {
        user.activated = true;
      }
      this.activateButtonText[i] = "Loading...";
      this.apiService.updateUser(user).subscribe((res: ResponseFormat) => {
        this.logger.debug("Update Profile - User by ID: " + JSON.stringify(res));
        if (res.success) {
          setTimeout(() => {
            this.getUsersList();
          }, 1000);
        } else {
          this.synergeService.handleError(res.message);
        }
      }, error => {
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      const selectOfficeDialog = this.dialog.open(SelectOfficeDialogComponent, {
        width: '600px'
      });
      selectOfficeDialog.afterClosed().subscribe(result => {
        if (result) {
          user.belongs_to = result.belongs_to;
          user.company_name = result.company_name;
          this.onActivate(user, i);
        }
      });
    }
  }

  onDelete(user: User, i: number) {
    this.logger.debug("On Delete: " + JSON.stringify(user));
    const confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      width: '600px',
      data: `Do you want to delete ${user.first_name} ${user.last_name}?`
    });
    confirmationDialog.afterClosed().subscribe(result => {
      if (result) {
        this.deleteButtonText[i] = "Loading...";
        this.apiService.deleteUser(user).subscribe((res: ResponseFormat) => {
          this.logger.debug("Update Profile - User by ID: " + JSON.stringify(res));
          if (res.success) {
            setTimeout(() => {
              this.getUsersList();
            }, 1000);
          } else {
            this.synergeService.handleError(res.message);
          }
        }, error => {
          this.synergeService.handleErrorOnAPI(error);
        });
      }
    });
  }

}

@Component({
  selector: 'select-office-dialog',
  templateUrl: './select-office-dialog.html'
})
export class SelectOfficeDialogComponent implements OnInit {
  clients: Clients[] = [];
  companyNames = [];
  belongsTo = "";
  companyName = "";
  message = "";

  constructor(public dialogRef: MatDialogRef<SelectOfficeDialogComponent>,
    private apiService: ApiService,
    private synergeService: SynergeService,
    private logger: LoggerService) { }

  ngOnInit() {
    this.apiService.getAllActiveClients().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got clients count: " + res.data.length);
      this.clients = res.data;
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  onBelongsToChange(belongsTo) {
    this.logger.debug("onBelongsToChange: " + belongsTo);
    if (belongsTo === 'SYNERGE_I') {
      this.companyNames = this.clients.filter(x => x.belongs_to === 'SYNERGE_I');
    } else {
      this.companyNames = this.clients.filter(x => x.belongs_to === 'SYNERGE_II');
    }
  }

  onSubmit() {
    if (this.belongsTo && this.companyName) {
      let res = {
        belongs_to: this.belongsTo,
        company_name: this.companyName
      }
      this.dialogRef.close(res);
    } else {
      this.message = "Please select all fields.";
    }
  }
}
