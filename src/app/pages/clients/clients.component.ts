import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/services/api.service';
import { SynergeService } from 'app/services/synerge.service';
import { MatDialog } from '@angular/material';
import { LoggerService } from 'app/services/logger.service';
import { ResponseFormat } from 'app/domains/response-format';
import { Clients } from 'app/domains/models/clients';
import { ConfirmationDialogComponent } from 'app/components/confirmation-dialog/confirmation-dialog.component';
import { ClientDialogComponent } from 'app/components/client-dialog/client-dialog.component';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {
  clients = [];
  deleteButtonText: string[] = [];

  constructor(private apiService: ApiService,
    private synergeService: SynergeService,
    public dialog: MatDialog,
    private logger: LoggerService) { }

  ngOnInit() {
    this.getClientsList();
  }

  getClientsList() {
    this.apiService.getAllClients().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got clients count: " + res.data.length);
      this.clients = res.data;
      this.clients.forEach((client: Clients, i: number) => {
        this.deleteButtonText[i] = 'Delete';
      });
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  onAdd() {
    const clientDialog = this.dialog.open(ClientDialogComponent, {
      width: '600px'
    });
    clientDialog.afterClosed().subscribe(result => {
      if (result) {
        this.getClientsList();
      }
    });
  }

  onEdit(client: Clients) {
    this.logger.debug("Edit Client: " + JSON.stringify(client));
    const clientDialog = this.dialog.open(ClientDialogComponent, {
      width: '600px',
      data: client._id
    });

    clientDialog.afterClosed().subscribe(result => {
      if (result) {
        this.getClientsList();
      }
    });
  }

  onDelete(client: Clients, i: number) {
    this.logger.debug("On Delete: " + JSON.stringify(client));
    const confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      width: '600px',
      data: `Do you want to delete ${client.company_name}?`
    });
    confirmationDialog.afterClosed().subscribe(result => {
      if (result) {
        this.deleteButtonText[i] = "Loading...";
        this.apiService.deleteClientById(client).subscribe((res: ResponseFormat) => {
          this.logger.debug("Client Deleted by ID: " + JSON.stringify(res));
          if (res.success) {
            setTimeout(() => {
              this.getClientsList();
            }, 1000);
          } else {
            this.synergeService.handleError(res.message);
          }
        }, error => {
          this.synergeService.handleErrorOnAPI(error);
        });
      }
    });
  }
}
