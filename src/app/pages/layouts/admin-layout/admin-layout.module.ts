import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';

import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSelectModule,
  MatProgressSpinnerModule,
  MatDialogModule,
  MatTableModule,
  MatListModule,
  MatIconModule,
  MatMenuModule,
  MatCardModule,
  MatTabsModule
} from '@angular/material';
import { NewsFeedComponent, NewsFeedLikesDialogComponent, NewsFeedCommentDialogComponent } from 'app/pages/news-feed/news-feed.component';
import { UsersComponent, SelectOfficeDialogComponent } from 'app/pages/users/users.component';
import { MyBookingComponent } from 'app/pages/my-booking/my-booking.component';
import { EventsComponent, EventLikesDialogComponent, EventCommentDialogComponent } from '../../events/events.component';
import { MyProfileComponent } from 'app/pages/my-profile/my-profile.component';
import { EditProfileComponent } from '../../edit-profile/edit-profile.component';
import { AllBookingsComponent, EditBookingDialogComponent, AddAdminBookingDialogComponent } from 'app/pages/all-bookings/all-bookings.component';
import { ClientsComponent } from 'app/pages/clients/clients.component';
import { MyVisitorsComponent } from 'app/pages/my-visitors/my-visitors.component';
import { ClientVisitorsComponent } from 'app/pages/client-visitors/client-visitors.component';
import { FeedbackComponent, FeedbackCommentDialogComponent } from 'app/pages/feedback/feedback.component';
import { SynergeFamilyComponent } from 'app/pages/synerge-family/synerge-family.component';
import { AchieversComponent } from 'app/pages/achievers/achievers.component';
import { UserInfoComponent } from 'app/pages/user-info/user-info.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatTableModule,
    MatListModule,
    MatIconModule,
    MatMenuModule,
    MatCardModule,
    MatTabsModule
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    TableListComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    UpgradeComponent,
    NewsFeedComponent,
    MyProfileComponent,
    UsersComponent,
    MyBookingComponent,
    EventsComponent,
    EditProfileComponent,
    AllBookingsComponent,
    SelectOfficeDialogComponent,
    NewsFeedLikesDialogComponent,
    NewsFeedCommentDialogComponent,
    EditBookingDialogComponent,
    AddAdminBookingDialogComponent,
    ClientsComponent,
    ClientVisitorsComponent,
    MyVisitorsComponent,
    FeedbackComponent,
    SynergeFamilyComponent,
    EventLikesDialogComponent,
    EventCommentDialogComponent,
    FeedbackCommentDialogComponent,
    AchieversComponent,
    UserInfoComponent
  ],
  entryComponents: [
    SelectOfficeDialogComponent,
    NewsFeedLikesDialogComponent,
    NewsFeedCommentDialogComponent,
    EditBookingDialogComponent,
    AddAdminBookingDialogComponent,
    EventLikesDialogComponent,
    EventCommentDialogComponent,
    FeedbackCommentDialogComponent
  ]
})

export class AdminLayoutModule {}
