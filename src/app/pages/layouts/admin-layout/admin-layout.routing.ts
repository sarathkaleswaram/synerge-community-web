import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { NewsFeedComponent } from 'app/pages/news-feed/news-feed.component';
import { UsersComponent } from 'app/pages/users/users.component';
import { MyBookingComponent } from 'app/pages/my-booking/my-booking.component';
import { EventsComponent } from 'app/pages/events/events.component';
import { MyProfileComponent } from 'app/pages/my-profile/my-profile.component';
import { EditProfileComponent } from 'app/pages/edit-profile/edit-profile.component';
import { AllBookingsComponent } from 'app/pages/all-bookings/all-bookings.component';
import { ClientsComponent } from 'app/pages/clients/clients.component';
import { ClientVisitorsComponent } from 'app/pages/client-visitors/client-visitors.component';
import { MyVisitorsComponent } from 'app/pages/my-visitors/my-visitors.component';
import { FeedbackComponent } from 'app/pages/feedback/feedback.component';
import { SynergeFamilyComponent } from 'app/pages/synerge-family/synerge-family.component';
import { AchieversComponent } from 'app/pages/achievers/achievers.component';
import { UserInfoComponent } from 'app/pages/user-info/user-info.component';

export const AdminLayoutRoutes: Routes = [
    // {
    //   path: '',
    //   children: [ {
    //     path: 'dashboard',
    //     component: DashboardComponent
    // }]}, {
    // path: '',
    // children: [ {
    //   path: 'userprofile',
    //   component: UserProfileComponent
    // }]
    // }, {
    //   path: '',
    //   children: [ {
    //     path: 'icons',
    //     component: IconsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'notifications',
    //         component: NotificationsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'maps',
    //         component: MapsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'typography',
    //         component: TypographyComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'upgrade',
    //         component: UpgradeComponent
    //     }]
    // }
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'table-list',     component: TableListComponent },
    { path: 'typography',     component: TypographyComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'maps',           component: MapsComponent },
    { path: 'notifications',  component: NotificationsComponent },
    { path: 'upgrade',        component: UpgradeComponent },

    // Menu Items
    { path: 'news-feed',        component: NewsFeedComponent },
    { path: 'my-profile',       component: MyProfileComponent },
    { path: 'clients',          component: ClientsComponent },
    { path: 'users',            component: UsersComponent },
    { path: 'my-booking',       component: MyBookingComponent },
    { path: 'client-visitors',  component: ClientVisitorsComponent },
    { path: 'all-bookings',     component: AllBookingsComponent },
    { path: 'my-visitors',      component: MyVisitorsComponent },
    { path: 'events',           component: EventsComponent },
    { path: 'feedback',         component: FeedbackComponent },
    { path: 'achievers',        component: AchieversComponent },
    { path: 'synerge-family',   component: SynergeFamilyComponent },
    { path: 'user-info',        component: UserInfoComponent },

    // Pages
    { path: 'edit-profile',     component: EditProfileComponent },
];
