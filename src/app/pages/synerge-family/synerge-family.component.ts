import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'app/domains/app-constants';
import { SynergeFamily } from 'app/domains/models/synergeFamily';
import { MatDialog } from '@angular/material';
import { SynergeService } from 'app/services/synerge.service';
import { ApiService } from 'app/services/api.service';
import { LoggerService } from 'app/services/logger.service';
import { ResponseFormat } from 'app/domains/response-format';
import { SynergeFamilyDialogComponent } from 'app/components/synerge-family-dialog/synerge-family-dialog.component';
import { ConfirmationDialogComponent } from 'app/components/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-synerge-family',
  templateUrl: './synerge-family.component.html',
  styleUrls: ['./synerge-family.component.scss']
})
export class SynergeFamilyComponent implements OnInit {
  synergeI: SynergeFamily[];
  synergeII: SynergeFamily[];
  isAdmin = localStorage.getItem(AppConstants.USER_ROLE) === 'ADMIN' ? true : false;

  constructor(public dialog: MatDialog,
    public synergeService: SynergeService,
    private apiService: ApiService,
    private logger: LoggerService) { }

  ngOnInit() {
    this.getAllSynergeFamily();
  }

  getAllSynergeFamily() {
    this.apiService.getAllSynergeFamilys().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got Total Synerge Familys count: " + res.data.length);
      if (res.success) {
        let totalSynergeFamily: SynergeFamily[] = res.data;
        this.synergeI = totalSynergeFamily.filter(x => x.belongs_to === "SYNERGE_I");
        this.logger.debug("SynergeI count: " + this.synergeI.length);
        this.synergeII = totalSynergeFamily.filter(x => x.belongs_to === "SYNERGE_II");
        this.logger.debug("SynergeII count: " + this.synergeII.length);
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  editSynergeFamily(synergeFamily: SynergeFamily) {
    const synergeFamilyDialog = this.dialog.open(SynergeFamilyDialogComponent, {
      width: '600px',
      data: synergeFamily._id
    });
    synergeFamilyDialog.afterClosed().subscribe(result => {
      if (result && result === "Success") {
        this.getAllSynergeFamily();
      }
    });
  }

  deleteSynergeFamily(synergeFamily: SynergeFamily) {
    const confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      width: '600px',
      data: `Do you want to delete ${synergeFamily.company_name}?`
    });
    confirmationDialog.afterClosed().subscribe(result => {
      if (result) {
        this.apiService.deleteSynergeFamilyById(synergeFamily._id).subscribe((res: ResponseFormat) => {
          this.logger.debug("SynergeFamily Deleted by ID: " + JSON.stringify(res));
          if (res.success) {
            setTimeout(() => {
              this.getAllSynergeFamily();
            }, 1000);
          } else {
            this.synergeService.handleError(res.message);
          }
        }, error => {
          this.synergeService.handleErrorOnAPI(error);
        });
      }
    });
  }

  onAdd() {
    const synergeFamilyDialog = this.dialog.open(SynergeFamilyDialogComponent, {
      width: '600px'
    });
    synergeFamilyDialog.afterClosed().subscribe(result => {
      if (result && result === "Success") {
        this.getAllSynergeFamily();
      }
    });
  }

}
