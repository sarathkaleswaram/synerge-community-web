import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SynergeFamilyComponent } from './synerge-family.component';

describe('SynergeFamilyComponent', () => {
  let component: SynergeFamilyComponent;
  let fixture: ComponentFixture<SynergeFamilyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SynergeFamilyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SynergeFamilyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
