import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'app/domains/app-constants';
import { Achiever } from 'app/domains/models/achievers';
import { MatDialog } from '@angular/material';
import { SynergeService } from 'app/services/synerge.service';
import { ApiService } from 'app/services/api.service';
import { LoggerService } from 'app/services/logger.service';
import { ResponseFormat } from 'app/domains/response-format';
import { AchieversDialogComponent } from 'app/components/achievers-dialog/achievers-dialog.component';
import { ConfirmationDialogComponent } from 'app/components/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-achievers',
  templateUrl: './achievers.component.html',
  styleUrls: ['./achievers.component.scss']
})
export class AchieversComponent implements OnInit {
  synergeIAchiever: Achiever[];
  synergeIIAchiever: Achiever[];
  isAdmin = localStorage.getItem(AppConstants.USER_ROLE) === 'ADMIN' ? true : false;

  constructor(public dialog: MatDialog,
    public synergeService: SynergeService,
    private apiService: ApiService,
    private logger: LoggerService) { }

  ngOnInit() {
    this.getAllAchiever();
  }

  getAllAchiever() {
    this.apiService.getAllAchievers().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got Total Achievers count: " + res.data.length);
      if (res.success) {
        let totalAchievers: Achiever[] = res.data;
        this.synergeIAchiever = totalAchievers.filter(x => x.belongs_to === "SYNERGE_I");
        this.logger.debug("Synerge I Achievers count: " + this.synergeIAchiever.length);
        this.synergeIIAchiever = totalAchievers.filter(x => x.belongs_to === "SYNERGE_II");
        this.logger.debug("Synerge II Achievers count: " + this.synergeIIAchiever.length);
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  editAchiever(achiever: Achiever) {
    const achieverDialog = this.dialog.open(AchieversDialogComponent, {
      width: '600px',
      data: achiever._id
    });
    achieverDialog.afterClosed().subscribe(result => {
      if (result && result === "Success") {
        this.getAllAchiever();
      }
    });
  }

  deleteAchiever(achiever: Achiever) {
    const confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      width: '600px',
      data: `Do you want to delete ${achiever.achiever}?`
    });
    confirmationDialog.afterClosed().subscribe(result => {
      if (result) {
        this.apiService.deleteAchieverById(achiever._id).subscribe((res: ResponseFormat) => {
          this.logger.debug("Achiever Deleted by ID: " + JSON.stringify(res));
          if (res.success) {
            setTimeout(() => {
              this.getAllAchiever();
            }, 1000);
          } else {
            this.synergeService.handleError(res.message);
          }
        }, error => {
          this.synergeService.handleErrorOnAPI(error);
        });
      }
    });
  }

  onAdd() {
    const achieverDialog = this.dialog.open(AchieversDialogComponent, {
      width: '600px'
    });
    achieverDialog.afterClosed().subscribe(result => {
      if (result && result === "Success") {
        this.getAllAchiever();
      }
    });
  }

}
