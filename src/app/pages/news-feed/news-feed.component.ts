import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NewsFeedDialogComponent } from 'app/components/news-feed-dialog/news-feed-dialog.component';
import { LoggerService } from 'app/services/logger.service';
import { SynergeService } from 'app/services/synerge.service';
import { ApiService } from 'app/services/api.service';
import { NewsFeed, Likes, Comments } from 'app/domains/models/newsFeed';
import { ResponseFormat } from 'app/domains/response-format';
import { AppConstants } from 'app/domains/app-constants';

@Component({
  selector: 'app-news-feed',
  templateUrl: './news-feed.component.html',
  styleUrls: ['./news-feed.component.scss']
})
export class NewsFeedComponent implements OnInit {
  newsFeeds: NewsFeed[];

  constructor(public dialog: MatDialog,
    public synergeService: SynergeService,
    private apiService: ApiService,
    private logger: LoggerService) { }

  ngOnInit() {
    this.getAllNewsFeed();
  }

  getAllNewsFeed() {
    this.apiService.getAllNewsFeeds().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got News Feed count: " + res.data.length);
      if (res.success) {
        this.newsFeeds = res.data;
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  getDidILike(newsFeed: NewsFeed) {
    return newsFeed.likes.find(x => x.by_user_id === localStorage.getItem(AppConstants.USER_ID));
  }

  onAdd() {
    const newsFeedDialog = this.dialog.open(NewsFeedDialogComponent, {
      width: '600px'
    });
    newsFeedDialog.afterClosed().subscribe(result => {
      if (result && result === "Success") {
        this.getAllNewsFeed();
      }
    });
  }

  onLike(newsFeed: NewsFeed) {
    if (this.getDidILike(newsFeed)) {
      newsFeed.likes.splice(newsFeed.likes.findIndex(x => x.by_user_id === localStorage.getItem(AppConstants.USER_ID)), 1);
    } else {
      let like: Likes = {
        by_user_id: localStorage.getItem(AppConstants.USER_ID),
        by_user_name: localStorage.getItem(AppConstants.USER_FULL_NAME),
        date: new Date()
      };
      newsFeed.likes.push(like);
    }
    this.apiService.updateNewsFeedById(newsFeed).subscribe((res: ResponseFormat) => {
      this.logger.debug("Update news feed result: " + JSON.stringify(res));
      if (res.success) {
        this.getAllNewsFeed();
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  onLikeList(newsFeed: NewsFeed) {
    this.dialog.open(NewsFeedLikesDialogComponent, {
      data: newsFeed.likes
    });
  }

  onComment(newsFeed: NewsFeed) {
    this.dialog.open(NewsFeedCommentDialogComponent, {
      data: newsFeed
    });
  }

}

@Component({
  selector: 'news-feed-likes-dialog',
  templateUrl: './news-feed-likes-dialog.html'
})
export class NewsFeedLikesDialogComponent {
  constructor(
    public synergeService: SynergeService,
    @Inject(MAT_DIALOG_DATA) public likes: Likes[],
    public dialogRef: MatDialogRef<NewsFeedLikesDialogComponent>) { }
}

@Component({
  selector: 'news-feed-comments-dialog',
  templateUrl: './news-feed-comments-dialog.html'
})
export class NewsFeedCommentDialogComponent {
  newComment = "";

  constructor(public dialogRef: MatDialogRef<NewsFeedCommentDialogComponent>,
    public synergeService: SynergeService,
    private apiService: ApiService,
    private logger: LoggerService,
    @Inject(MAT_DIALOG_DATA) public newsFeed: NewsFeed) { }

  getNewsFeed(newsFeed: NewsFeed) {
    this.apiService.getNewsFeedById(newsFeed._id).subscribe((res: ResponseFormat) => {
      if (res.success) {
        this.newsFeed = res.data;
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  onSend() {
    let comment: Comments = {
      comment: this.newComment,
      by_user_id: localStorage.getItem(AppConstants.USER_ID),
      by_user_name: localStorage.getItem(AppConstants.USER_FULL_NAME),
      date: new Date()
    };
    this.newComment = "";
    this.newsFeed.comments.push(comment);
    this.apiService.updateNewsFeedById(this.newsFeed).subscribe((res: ResponseFormat) => {
      this.logger.debug("Update news feed result: " + JSON.stringify(res));
      if (res.success) {
        this.getNewsFeed(this.newsFeed);
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }
}