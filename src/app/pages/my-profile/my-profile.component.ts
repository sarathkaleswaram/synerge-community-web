import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/services/api.service';
import { AppConstants } from 'app/domains/app-constants';
import { LoggerService } from 'app/services/logger.service';
import { SynergeService } from 'app/services/synerge.service';
import { User } from 'app/domains/models/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent implements OnInit {
  user: User;

  constructor(private apiService: ApiService,
    public synergeService: SynergeService,
    private router: Router,
    private logger: LoggerService) { }

  ngOnInit() {
    this.apiService.getUserById(localStorage.getItem(AppConstants.USER_ID)).subscribe(data => {
      this.logger.debug("My Profile - User by ID: " + JSON.stringify(data));
      this.user = data.data;
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }
  
  onEditClick(id) {
    this.router.navigate(["/edit-profile"], { queryParams: { user_id: id }});
  }

}
