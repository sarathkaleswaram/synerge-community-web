import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyVisitorsComponent } from './my-visitors.component';

describe('MyVisitorsComponent', () => {
  let component: MyVisitorsComponent;
  let fixture: ComponentFixture<MyVisitorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyVisitorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyVisitorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
