import { Component, OnInit } from '@angular/core';
import { SynergeService } from 'app/services/synerge.service';
import { ApiService } from 'app/services/api.service';
import { LoggerService } from 'app/services/logger.service';
import { ClientVisitors } from 'app/domains/models/clientVisitors';
import { ResponseFormat, SocketEventFormat } from 'app/domains/response-format';
import { SocketService } from 'app/services/socket.service';
import { AppConstants } from 'app/domains/app-constants';

@Component({
  selector: 'app-my-visitors',
  templateUrl: './my-visitors.component.html',
  styleUrls: ['./my-visitors.component.scss']
})
export class MyVisitorsComponent implements OnInit {
  myVisitors: ClientVisitors[];

  constructor(public synergeService: SynergeService,
    private apiService: ApiService,
    private socketService: SocketService,
    private logger: LoggerService) { }

  ngOnInit() {
    this.socketService.resetDotNotification();
    this.getAllClientVisitors();
    this.socketService.socketObservable.subscribe((data: SocketEventFormat) => {
      if (data.type === AppConstants.TYPE_CLIENT_VISITOR) {
        this.logger.debug("Socket ClientVisitors component update");
        this.getAllClientVisitors();        
      }
    });
  }
  
  getAllClientVisitors() {
    this.apiService.getAllClientVisitors().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got My Visitors count: " + res.data.length);
      if (res.success) {
        this.myVisitors = res.data;
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }
}
