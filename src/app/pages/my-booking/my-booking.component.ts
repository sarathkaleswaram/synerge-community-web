import { Component, OnInit } from '@angular/core';
import { LoggerService } from 'app/services/logger.service';
import { ApiService } from 'app/services/api.service';
import { SynergeService } from 'app/services/synerge.service';
import { MatDialog } from '@angular/material';
import { AddBookingDialogComponent } from 'app/components/add-booking-dialog/add-booking-dialog.component';
import { ResponseFormat, SocketEventFormat } from 'app/domains/response-format';
import { Booking } from 'app/domains/models/bookings';
import { AppConstants } from 'app/domains/app-constants';
import { SocketService } from 'app/services/socket.service';

@Component({
  selector: 'app-my-booking',
  templateUrl: './my-booking.component.html',
  styleUrls: ['./my-booking.component.scss']
})
export class MyBookingComponent implements OnInit {
  bookings: Booking[];

  constructor(public dialog: MatDialog,
    public synergeService: SynergeService,
    private socketService: SocketService,
    private apiService: ApiService,
    private logger: LoggerService) { }

  ngOnInit() {
    this.socketService.resetDotNotification();
    this.getBookings();
    this.socketService.socketObservable.subscribe((data: SocketEventFormat) => {
      if (data.type === AppConstants.TYPE_BOOKING) {
        this.logger.debug("Socket My Bookings component update");
        this.getBookings();        
      }
    });
  }

  getBookings() {    
    this.apiService.getAllBookings().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got Bookings count: " + res.data.length);
      if (res.success) {
        this.bookings = res.data;
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  // onDelete(booking: Booking) {
  //   this.apiService.deleteBookingById(booking._id).subscribe((res: ResponseFormat) => {
  //     this.logger.debug("Delete Booking: " + JSON.stringify(res));
  //     if (res.success) {
  //       this.getBookings();
  //     } else {
  //       this.synergeService.handleError(res.message);
  //     }
  //   }, error => {
  //     this.synergeService.handleErrorOnAPI(error);
  //   });
  // }

  onAdd() {
    let data = {
      belongs_to: localStorage.getItem(AppConstants.USER_BELONGS_TO),
      company_name: localStorage.getItem(AppConstants.USER_COMPANY_NAME)
    }
    const bookingDialog = this.dialog.open(AddBookingDialogComponent, {
      width: '800px',
      data: data
    });
    bookingDialog.afterClosed().subscribe(result => {
      this.getBookings();
    });
  }

}
