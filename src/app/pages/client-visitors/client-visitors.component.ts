import { Component, OnInit } from '@angular/core';
import { ClientVisitors } from 'app/domains/models/clientVisitors';
import { MatDialog } from '@angular/material';
import { SynergeService } from 'app/services/synerge.service';
import { ApiService } from 'app/services/api.service';
import { LoggerService } from 'app/services/logger.service';
import { ResponseFormat, SocketEventFormat } from 'app/domains/response-format';
import { ConfirmationDialogComponent } from 'app/components/confirmation-dialog/confirmation-dialog.component';
import { AddClientVisitorsDialogComponent } from 'app/components/add-client-visitors-dialog/add-client-visitors-dialog.component';
import { AppConstants } from 'app/domains/app-constants';
import { SocketService } from 'app/services/socket.service';

@Component({
  selector: 'app-client-visitors',
  templateUrl: './client-visitors.component.html',
  styleUrls: ['./client-visitors.component.scss']
})
export class ClientVisitorsComponent implements OnInit {
  clientVisitors: ClientVisitors[];
  deleteButtonText: string[] = [];

  constructor(public dialog: MatDialog,
    public synergeService: SynergeService,
    private socketService: SocketService,
    private apiService: ApiService,
    private logger: LoggerService) { }

  ngOnInit() {
    this.socketService.resetDotNotification();
    this.getClientVisitors();
    this.socketService.socketObservable.subscribe((data: SocketEventFormat) => {
      if (data.type === AppConstants.TYPE_CLIENT_VISITOR) {
        this.logger.debug("Socket ClientVisitors component update");
        this.getClientVisitors();        
      }
    });
  }

  getClientVisitors() {
    this.apiService.getAllClientVisitors().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got Client Visitors count: " + res.data.length);
      if (res.success) {
        this.clientVisitors = res.data;
        this.clientVisitors.forEach((clientVistors: ClientVisitors, i: number) => {
          this.deleteButtonText[i] = 'Delete';
        });
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }
  
  onDelete(clientVistors: ClientVisitors, i: number) {
    this.logger.debug("On Delete: " + JSON.stringify(clientVistors));
    const confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      width: '600px',
      data: `Do you want to delete?`
    });
    confirmationDialog.afterClosed().subscribe(result => {
      if (result) {
        this.deleteButtonText[i] = "Loading...";
        this.apiService.deleteClientVisitorById(clientVistors).subscribe((res: ResponseFormat) => {
          this.logger.debug("Client Vistors Deleted by ID: " + JSON.stringify(res));
          if (res.success) {
            setTimeout(() => {
              this.getClientVisitors();
            }, 1000);
          } else {
            this.synergeService.handleError(res.message);
          }
        }, error => {
          this.synergeService.handleErrorOnAPI(error);
        });
      }
    });
  }

  onAdd() {
    const clientVisitorsDialog = this.dialog.open(AddClientVisitorsDialogComponent, {
      width: '600px'
    });
    clientVisitorsDialog.afterClosed().subscribe(result => {
      this.getClientVisitors();
    });
  }
}
