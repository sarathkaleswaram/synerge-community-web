import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientVisitorsComponent } from './client-visitors.component';

describe('ClientVisitorsComponent', () => {
  let component: ClientVisitorsComponent;
  let fixture: ComponentFixture<ClientVisitorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientVisitorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientVisitorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
