import { Injectable } from '@angular/core';
import { LoggerService } from './logger.service';
import * as $jQuery from 'jquery';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

declare var $: any;

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private logger: LoggerService,
    private router: Router,
    private snackBar: MatSnackBar) { }

  openBetaVersionSnackBar() {
    // this.snackBar.open('You are using BETA version of SYNERGE Community.', 'Understood', {
    //   verticalPosition: 'top',
    //   horizontalPosition: 'center',
    //   duration: 3000,
    // });
    $.notify({
      icon: 'notifications',
      title: 'Note:',
      message: 'You are using BETA version of SYNERGE Community.',
      target: '_self'// _blank, _self, _parent, _top
    }, {
      type: 'danger', // '','info','success','warning','danger'
      timer: 3,
      placement: {
        from: 'top', // 'top', 'bottom'
        align: 'center' // 'left', 'center', 'right'
      },
      template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
        '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
        '<i class="material-icons" data-notify="icon">info</i> ' +
        '<span data-notify="title">{1}</span> ' +
        '<span data-notify="message">{2}</span>' +
        '<div class="progress" data-notify="progressbar">' +
        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
        '</div>' +
        '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
  }

  openSnackBar(message: string, url: string) {
    this.logger.debug('Open Snackbar message: ' + message + ', url: ' + url);
    let snackBarRef = this.snackBar.open(message, url ? 'Open' : '', {
      verticalPosition: 'top',
      horizontalPosition: 'center',
      duration: 5000,
    });
    snackBarRef.onAction().subscribe(() => {
      this.logger.debug("Snack-bar Action clicked redirecting: " + url);
      this.router.navigateByUrl(url);
    });
  }

  openNotification(message: string, url: string) {
    this.logger.debug('Open Snackbar message: ' + message + ', url: ' + url);
    $.notify({
      icon: 'notifications',
      title: '',
      message: message,
      url: url,
      target: '_self'// _blank, _self, _parent, _top
    }, {
      type: 'danger', // '','info','success','warning','danger'
      timer: 10,
      placement: {
        from: 'top', // 'top', 'bottom'
        align: 'right' // 'left', 'center', 'right'
      },
      template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
        '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
        '<i class="material-icons" data-notify="icon">notifications</i> ' +
        '<span data-notify="title">{1}</span> ' +
        '<span data-notify="message">{2}</span>' +
        '<div class="progress" data-notify="progressbar">' +
        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
        '</div>' +
        '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
  }
}
