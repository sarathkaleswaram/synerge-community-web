import { Injectable } from '@angular/core';

import * as config from '../../../config.json';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  loglevel: string = config.logLevel;

  constructor() { }

  debug(content: string) {
      if (this.loglevel == 'debug') {
          console.debug(content);
      }
  }

  info(content: string) {
      if (this.loglevel == 'debug' || this.loglevel == 'info') {
          console.info(content);
      }
  }

  error(content: string) {
      if (this.loglevel == 'debug' || this.loglevel == 'info' || this.loglevel == 'error') {
          console.error(content);
      }
  }
}
