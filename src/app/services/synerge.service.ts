import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LoggerService } from './logger.service';
import * as $jQuery from 'jquery';
import { AppConstants } from 'app/domains/app-constants';
import * as config from '../../../config.json';
import { SocketService } from './socket.service';
import { OverlayContainer } from '@angular/cdk/overlay';
import { MatDialog } from '@angular/material';
import { AlertDialogComponent } from 'app/components/alert-dialog/alert-dialog.component';
import { ConfirmationDialogComponent } from 'app/components/confirmation-dialog/confirmation-dialog.component';

declare var $: any;

@Injectable({
  providedIn: 'root'
})
export class SynergeService {
  basePath: string = config.basePath;
  doLogout: boolean = false;
  darkEdition: boolean;

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private socketService: SocketService,
    public overlayContainer: OverlayContainer,
    private logger: LoggerService
  ) { }

  logout() {
    this.logger.debug("Logging Out");
    this.socketService.disconnectSocket();
    // localStorage.clear();
    localStorage.removeItem(AppConstants.ROLE_ADMIN);
    localStorage.removeItem(AppConstants.TOKEN);
    localStorage.removeItem(AppConstants.ROUTES);
    localStorage.removeItem(AppConstants.USER_FULL_NAME);
    localStorage.removeItem(AppConstants.USER_ID);
    localStorage.removeItem(AppConstants.USER_ROLE);
    localStorage.removeItem(AppConstants.USER_BELONGS_TO);
    localStorage.removeItem(AppConstants.USER_COMPANY_NAME);
    this.router.navigate(['/login']);
  }

  onErrorModalClick() {
    if (this.doLogout) {
      this.logout();
      this.doLogout = false;
    }
  }

  handleErrorOnAPI(error) {
    console.log("Error: ", error);
    if (!error.error) return;
    // $('#errorModalLabel').text('Alert');
    // $('#errorModalContent').text(error.error ? error.error.message : error.statusText);
    // $('#errorModal').modal('show');
    let message = error.error ? error.error.message : error.statusText;
    if (error.error.message == AppConstants.ERROR_TOKEN_INVALID) {
      this.doLogout = true;
      message += ' Logging out.';
      // $('#errorModalSubContent').text('Logging out.');
    }
    if (!message)
      message = 'Unknow error occurred. Please try again later.';
    this.logger.debug('Error message: ' + message);
    const alertDialog = this.dialog.open(AlertDialogComponent, {
      width: '600px',
      data: message
    });
    alertDialog.afterClosed().subscribe(() => {
      if (this.doLogout) {
        this.logout();
        this.doLogout = false;
      }
    });
  }

  handleError(message: string) {
    this.logger.debug('Error - Param Missing: ' + message);
    if (!message)
      message = 'Unknow error occurred. Please try again later.';
    this.dialog.open(AlertDialogComponent, {
      width: '600px',
      data: message
    });
  }

  onUserInfo(user_id: string) {
    this.router.navigate(['/user-info'], { queryParams: { user_id: user_id }});
  }

  confirmLogout() {
    const confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      width: '600px',
      data: `Do you want to Logout?`
    });
    confirmationDialog.afterClosed().subscribe((result) => {
      if (result) {
        this.logout();
      }
    });
  }

  onThemeChange() {
    this.logger.debug('Theme update:' + !this.darkEdition);
    if (this.darkEdition) {
      this.overlayContainer.getContainerElement().classList.add('material-dark-edition');
      localStorage.setItem(AppConstants.DARK_EDITION_ACTIVE, 'true');
    } else {
      this.overlayContainer.getContainerElement().classList.remove('material-dark-edition');
      localStorage.setItem(AppConstants.DARK_EDITION_ACTIVE, 'false');
    }
  }
}
