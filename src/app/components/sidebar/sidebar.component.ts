import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'app/domains/app-constants';
import { SynergeService } from 'app/services/synerge.service';
import { SocketService } from 'app/services/socket.service';
import { version } from '../../../../package.json';

declare const $: any;
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/news-feed', title: 'News Feed', icon: 'dashboard', class: '' },
  { path: '/my-profile', title: 'My Profile', icon: 'person', class: '' },
  { path: '/clients', title: 'Clients', icon: 'person', class: '' },
  { path: '/users', title: 'Users', icon: 'person', class: '' },
  { path: '/all-bookings', title: 'All Bookings', icon: 'event', class: '' },
  { path: '/client-visitors', title: 'Client Visitors', icon: 'notifications', class: '' },
  { path: '/my-booking', title: 'My Bookings', icon: 'event', class: '' },
  { path: '/my-visitors', title: 'My Visitors', icon: 'library_books', class: '' },
  { path: '/events', title: 'Events', icon: 'sports_esports', class: '' },
  { path: '/feedback', title: 'Feedback/Suggestions', icon: 'content_paste', class: '' },
  { path: '/achievers', title: 'Achievers', icon: 'emoji_events', class: '' },
  { path: '/synerge-family', title: 'Synerge Family', icon: 'bubble_chart', class: '' },
  // { path: '/dashboard', title: 'Dashboard', icon: 'dashboard', class: '' },
  // { path: '/user-profile', title: 'User Profile', icon: 'person', class: '' },
  // { path: '/table-list', title: 'Table List', icon: 'content_paste', class: '' },
  // { path: '/typography', title: 'Typography', icon: 'library_books', class: '' },
  // { path: '/icons', title: 'Icons', icon: 'bubble_chart', class: '' },
  // { path: '/maps', title: 'Maps', icon: 'location_on', class: '' },
  // { path: '/notifications', title: 'Notifications', icon: 'notifications', class: '' },
  // { path: '/upgrade', title: 'Upgrade to PRO', icon: 'unarchive', class: 'active-pro' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems = [];
  userName: string;
  version: string = version;

  constructor(
    public synergeService: SynergeService,
    public socketService: SocketService
  ) {
    this.userName = localStorage.getItem(AppConstants.USER_FULL_NAME);
  }

  ngOnInit() {
    let routes: [] = JSON.parse(localStorage.getItem(AppConstants.ROUTES));
    routes.forEach(route => {
      let menu = ROUTES.find(menu => menu.title == route);
      if (menu) this.menuItems.push(menu);
    });
  }

  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };
}
