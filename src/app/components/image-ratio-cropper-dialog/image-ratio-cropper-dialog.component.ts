import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
  selector: 'app-image-ratio-cropper-dialog',
  templateUrl: './image-ratio-cropper-dialog.component.html',
  styleUrls: ['./image-ratio-cropper-dialog.component.scss']
})
export class ImageRatioCropperDialogComponent implements OnInit {
  aspectRatio = 4 / 4;

  imageChangedEvent: any = '';
  croppedImage: any = '';

  uploadedFile: any;
  croppedFile: any;

  constructor(
    public dialogRef: MatDialogRef<ImageRatioCropperDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit() {
    if (this.data.aspectRatio)
      this.aspectRatio = this.data.aspectRatio;
    this.imageChangedEvent = this.data.imageEvent;
    this.uploadedFile = this.data.imageEvent.target.files[0];
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  base64ToFile(data, filename) {
    const arr = data.split(',');
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    let u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  }

  imageLoaded() {
    // show cropper
  }

  cropperReady() {
    // cropper ready
  }

  loadImageFailed() {
    // show message
  }

  submitCrop() {
    // Data URI (base64) to File to send backend
    this.croppedFile = this.base64ToFile(this.croppedImage, this.uploadedFile.name);
    this.dialogRef.close({ croppedImage: this.croppedImage, croppedFile: this.croppedFile });
  }

}
