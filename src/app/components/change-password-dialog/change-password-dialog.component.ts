import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ValidatorService } from 'app/services/validator.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { User } from 'app/domains/models/user';
import * as md5 from 'md5';
import { ApiService } from 'app/services/api.service';
import { SynergeService } from 'app/services/synerge.service';
import { LoggerService } from 'app/services/logger.service';
import { ResponseFormat } from 'app/domains/response-format';

@Component({
  selector: 'app-change-password-dialog',
  templateUrl: './change-password-dialog.component.html',
  styleUrls: ['./change-password-dialog.component.scss']
})
export class ChangePasswordDialogComponent implements OnInit {
  changePasswordForm: FormGroup;
  buttonText = "Confirm";

  constructor(
    private formBuilder: FormBuilder,
    private synergeService: SynergeService,
    private apiService: ApiService,
    private logger: LoggerService,
    public dialogRef: MatDialogRef<ChangePasswordDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public user: User) { }

  ngOnInit() {
    this.changePasswordForm = this.formBuilder.group({
      oldPassword: new FormControl(this.user.password),
      currentPassword: new FormControl('', [Validators.required, ValidatorService.validateOldPassword]),
      newPassword: new FormControl('', [Validators.required, Validators.minLength(6)]),
      confirmPassword: new FormControl('', [Validators.required, ValidatorService.validateNewPassword])
    });
  }

  onSubmit() {
    if (this.changePasswordForm.valid) {
      this.buttonText = "Loading...";
      this.user.password = md5(this.changePasswordForm.value.newPassword);
      this.apiService.updateUser(this.user).subscribe((res: ResponseFormat) => {
        this.logger.debug("Change Password: " + JSON.stringify(res));
        if (res.success) {
          this.buttonText = "Success";
          setTimeout(() => {
            this.dialogRef.close(res.data);
          }, 1000);
        } else {
          this.buttonText = "Confirm";
          this.synergeService.handleError(res.message);
        }
      }, error => {
        this.buttonText = "Confirm";
        this.synergeService.handleErrorOnAPI(error);
      });
    }
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.changePasswordForm.controls[controlName].hasError(errorName);
  }

}
