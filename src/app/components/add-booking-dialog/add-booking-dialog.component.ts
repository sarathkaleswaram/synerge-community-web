import { Component, OnInit, Inject } from '@angular/core';
import { SynergeService } from 'app/services/synerge.service';
import { ApiService } from 'app/services/api.service';
import { LoggerService } from 'app/services/logger.service';
import { Booking } from 'app/domains/models/bookings';
import { AppConstants } from 'app/domains/app-constants';
import { ResponseFormat } from 'app/domains/response-format';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { BookingTimeDialogComponent } from '../booking-time-dialog/booking-time-dialog.component';

@Component({
  selector: 'app-add-booking-dialog',
  templateUrl: './add-booking-dialog.component.html',
  styleUrls: ['./add-booking-dialog.component.scss']
})
export class AddBookingDialogComponent implements OnInit {
  booking: Booking;

  selectedType: string;
  selectedDate: Date;
  selectedTime: string;

  bookingNote: string;

  discussionRoomString = "";

  minDate = new Date();

  // discussionRoom: boolean = false;
  // discussionRoomDate: Date;
  // discussionRoomTime: string;

  // conferenceRoom: boolean = false;
  // conferenceRoomDate: Date;
  // conferenceRoomTime: string;

  // pantry: boolean = false;
  // pantryDate: Date;
  // pantryTime: string;

  // coffeeTea: boolean = false;
  // coffeeTeaItems: string[] = [];
  // items = [{ item: "" }];

  // coffeeTeas = [
  //   'Cappachino',
  //   'Esspresso',
  //   'Latte Coffee',
  //   'Strong Coffee',
  //   'Boiled Milk',
  //   'Lemon Tea',
  //   'Hot Water',
  //   'Cold Water'
  // ];

  // timeSlots = [
  //   "08:00 AM",
  //   "08:30 AM",
  //   "09:00 AM",
  //   "09:30 AM",
  //   "10:00 AM",
  //   "10:30 AM",
  //   "11:00 AM",
  //   "11:30 AM",
  //   "12:00 PM",
  //   "12:30 PM",
  //   "01:00 PM",
  //   "01:30 PM",
  //   "02:00 PM",
  //   "02:30 PM",
  //   "03:00 PM",
  //   "03:30 PM",
  //   "04:00 PM",
  //   "04:30 PM",
  //   "05:00 PM",
  //   "05:30 PM",
  //   "06:00 PM"
  // ];

  buttonText = "Submit";
  message: string = "";
  showSuccessMessage: boolean = false;

  belongs_to = "";
  company_name = "";

  constructor(public dialog: MatDialog,
    public synergeService: SynergeService,
    private apiService: ApiService,
    private logger: LoggerService,
    @Inject(MAT_DIALOG_DATA) public data) {
    this.logger.debug("Add Booking data: " + JSON.stringify(data));
    this.belongs_to = data.belongs_to;
    this.company_name = data.company_name;
    if (this.belongs_to === 'SYNERGE_I') {
      this.discussionRoomString = 'Discussion room / Training room';
    } else {
      this.discussionRoomString = 'Discussion room';
    }
  }

  ngOnInit() {
  }

  onDateChange(event, booking_type) {
    let data = {
      booking_type: booking_type,
      booking_date: event.value,
      belongs_to: this.belongs_to
    }
    const dialogRef = this.dialog.open(BookingTimeDialogComponent, { width: '600px', data: data });

    dialogRef.afterClosed().subscribe(result => {
      this.logger.debug("Booking time dialog result: " + result);
      this.selectedTime = result;
    });
  }

  onSubmit() {
    this.message = "";
    if (this.selectedType && this.selectedDate && this.selectedTime) {
      this.buttonText = "Loading...";
      this.booking = {
        by_user_id: localStorage.getItem(AppConstants.USER_ID),
        by_user_name: localStorage.getItem(AppConstants.USER_FULL_NAME),
        belongs_to: this.belongs_to,
        company_name: this.company_name,
        booking_created_date: new Date(),
        booking_type: this.selectedType,
        booking_date: this.selectedDate,
        booking_time: this.selectedTime,
        status: "Request Pending",
        booking_note: this.bookingNote
      };
      this.apiService.addBooking(this.booking).subscribe((res: ResponseFormat) => {
        this.logger.debug("Booking Saved data: " + JSON.stringify(res));
        if (res.success) {
          this.buttonText = "Success";
          this.showSuccessMessage = true;
        } else {
          this.buttonText = "Submit";
          this.synergeService.handleError(res.message);
        }
      }, error => {
        this.buttonText = "Submit";
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.message = "Please select a Booking with Date and Time.";
    }
  }

  // onSubmit() {
  //   this.message = "";
  //   if (this.discussionRoom && this.discussionRoomDate && this.discussionRoomTime ||
  //     this.conferenceRoom && this.conferenceRoomDate && this.conferenceRoomTime ||
  //     this.pantry && this.pantryDate && this.pantryTime ||
  //     this.coffeeTea && this.coffeeTeaItems.length > 0) {
  //     this.buttonText = "Loading...";
  //     let booking_data = [];
  //     if (this.discussionRoom) {
  //       booking_data.push({ key: 'Discussion room', date: this.discussionRoomDate, time: this.discussionRoomTime });
  //     }
  //     if (this.conferenceRoom) {
  //       booking_data.push({ key: 'Conference room', date: this.conferenceRoomDate, time: this.conferenceRoomTime });
  //     }
  //     if (this.pantry) {
  //       booking_data.push({ key: 'Pantry', date: this.pantryDate, time: this.pantryTime });
  //     }
  //     if (this.coffeeTea && this.coffeeTeaItems.length > 0) {
  //       booking_data.push({ key: 'Coffee/Tea', value: this.coffeeTeaItems });
  //     }
  //     this.booking = {
  //       by_user_id: localStorage.getItem(AppConstants.USER_ID),
  //       by_user_name: localStorage.getItem(AppConstants.USER_FULL_NAME),
  //       belongs_to: this.belongs_to,
  //       company_name: this.company_name,
  //       requested_date: new Date(),
  //       booking_data: booking_data,
  //       status: "Request Pending"
  //     };
  //     this.logger.debug("Booking data: " + JSON.stringify(booking_data));
  //     this.apiService.addBooking(this.booking).subscribe((res: ResponseFormat) => {
  //       this.logger.debug("Booking Saved data: " + JSON.stringify(res));
  //       if (res.success) {
  //         this.buttonText = "Success";
  //         this.showSuccessMessage = true;
  //       } else {
  //         this.buttonText = "Submit";
  //         this.synergeService.handleError(res.message);
  //       }
  //     }, error => {
  //       this.buttonText = "Submit";
  //       this.synergeService.handleErrorOnAPI(error);
  //     });
  //   } else {
  //     this.message = "Please select Booking Date and Time.";
  //   }
  // }

  // addRow(i: number) {
  //   let item = { item: "" };
  //   if (this.items.length <= i + 1) {
  //     this.items.splice(i + 1, 0, item);
  //   }
  // }

  // deleteRow($event, item, i) {
  //   if (i == 0) {
  //     this.foodBeverages = false;
  //     return;
  //   }
  //   let index = this.items.indexOf(item);
  //   if ($event.which == 1) {
  //     this.items.splice(index, 1);
  //   }
  // }

}
