import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { LoggerService } from 'app/services/logger.service';
import { ApiService } from 'app/services/api.service';
import { SynergeService } from 'app/services/synerge.service';
import { Feedback } from 'app/domains/models/feedback';
import { ResponseFormat } from 'app/domains/response-format';

@Component({
  selector: 'app-edit-feedback-dialog',
  templateUrl: './edit-feedback-dialog.component.html',
  styleUrls: ['./edit-feedback-dialog.component.scss']
})
export class EditFeedbackDialogComponent implements OnInit {
  feedback: Feedback;
  buttonText = "Submit";
  message: string = "";

  constructor(private synergeService: SynergeService,
    private apiService: ApiService,
    private logger: LoggerService,
    public dialogRef: MatDialogRef<EditFeedbackDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public feedbackId) { }

  ngOnInit() {
    this.apiService.getFeedbackById(this.feedbackId).subscribe(data => {
      this.logger.debug("Edit Feedback - Feedback by ID: " + JSON.stringify(data));
      this.feedback = data.data;
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  onSubmit() {
    this.message = "";
    if (this.feedback.feedback == "") {
      this.message = "Please enter all fields.";
      return;
    }
    this.apiService.updateFeedbackById(this.feedback).subscribe((res: ResponseFormat) => {
      this.logger.debug("Feedback Updated data: " + JSON.stringify(res));
      if (res.success) {
        this.buttonText = "Success";
        setTimeout(() => {
          this.dialogRef.close("Success");
        }, 1000);
      } else {
        this.buttonText = "Submit";
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.buttonText = "Submit";
      this.synergeService.handleErrorOnAPI(error);
    });
  }

}
