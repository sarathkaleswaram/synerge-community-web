import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFeedbackDialogComponent } from './edit-feedback-dialog.component';

describe('EditFeedbackDialogComponent', () => {
  let component: EditFeedbackDialogComponent;
  let fixture: ComponentFixture<EditFeedbackDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditFeedbackDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFeedbackDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
