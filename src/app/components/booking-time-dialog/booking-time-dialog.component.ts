import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { LoggerService } from 'app/services/logger.service';
import { ApiService } from 'app/services/api.service';
import { SynergeService } from 'app/services/synerge.service';
import { ResponseFormat } from 'app/domains/response-format';

@Component({
  selector: 'app-booking-time-dialog',
  templateUrl: './booking-time-dialog.component.html',
  styleUrls: ['./booking-time-dialog.component.scss']
})
export class BookingTimeDialogComponent implements OnInit {
  selectedDate: Date;
  belongs_to = "";
  booking_type = "";
  selectedSlot: string;
  timeSlots = [
    {
      time: "08:00 AM - 09:00 AM",
      selected: false,
      booked: false
    },
    {
      time: "09:00 AM - 10:00 AM",
      selected: false,
      booked: false
    },
    {
      time: "10:00 AM - 11:00 AM",
      selected: false,
      booked: false
    },
    {
      time: "11:00 AM - 12:00 PM",
      selected: false,
      booked: false
    },
    {
      time: "12:00 PM - 01:00 PM",
      selected: false,
      booked: false
    },
    {
      time: "01:00 PM - 02:00 PM",
      selected: false,
      booked: false
    },
    {
      time: "02:00 PM - 03:00 PM",
      selected: false,
      booked: false
    },
    {
      time: "03:00 PM - 04:00 PM",
      selected: false,
      booked: false
    },
    {
      time: "04:00 PM - 05:00 PM",
      selected: false,
      booked: false
    },
    {
      time: "05:00 PM - 06:00 PM",
      selected: false,
      booked: false
    },
    {
      time: "06:00 PM - 07:00 PM",
      selected: false,
      booked: false
    },
    {
      time: "07:00 PM - 08:00 PM",
      selected: false,
      booked: false
    }
  ];

  constructor(public dialogRef: MatDialogRef<BookingTimeDialogComponent>,
    public synergeService: SynergeService,
    private apiService: ApiService,
    private logger: LoggerService,
    @Inject(MAT_DIALOG_DATA) public data) {
    this.logger.debug("Booking time data: " + JSON.stringify(data));
    this.selectedDate = data.booking_date;
    this.belongs_to = data.belongs_to;
    this.booking_type = data.booking_type;
    this.apiService.getBookingAvailableSlots(data).subscribe((res: ResponseFormat) => {
      this.logger.debug("Booking Available Slots data: " + JSON.stringify(res));
      if (res.success) {
        res.data.forEach(data => {
          let index = this.timeSlots.findIndex(x => x.time === data.booking_time);
          if (index >= 0) {
            this.logger.debug("Some Slots booked for Discussion room");
            this.timeSlots[index].booked = true;
          }
        });
        // if (this.booking_type === "Discussion room") {
        //   res.data.forEach(data => {
        //     data.booking_data.forEach(bookingData => {
        //       if (bookingData.key === "Discussion room") {
        //         let index = this.timeSlots.findIndex(x => x.time === bookingData.time);
        //         if (index >= 0) {
        //           this.logger.debug("Some Slots booked for Discussion room");
        //           this.timeSlots[index].booked = true;
        //         }
        //       }
        //     });
        //   });
        // } else if (this.booking_type === "Conference room") {
        //   res.data.forEach(data => {
        //     data.booking_data.forEach(bookingData => {
        //       if (bookingData.key === "Conference room") {
        //         let index = this.timeSlots.findIndex(x => x.time === bookingData.time);
        //         if (index >= 0) {
        //           this.logger.debug("Some Slots booked for Conference room");
        //           this.timeSlots[index].booked = true;
        //         }
        //       }
        //     });
        //   });
        // } else {
        //   res.data.forEach(data => {
        //     data.booking_data.forEach(bookingData => {
        //       if (bookingData.key === "Pantry") {
        //         let index = this.timeSlots.findIndex(x => x.time === bookingData.time);
        //         if (index >= 0) {
        //           this.logger.debug("Some Slots booked for Pantry");
        //           this.timeSlots[index].booked = true;
        //         }
        //       }
        //     });
        //   });
        // }
        this.logger.debug("TimeSlots after filter: " + JSON.stringify(this.timeSlots));
      } else {
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
  }

  ngOnInit() {

  }

  onSelect(slot) {
    this.timeSlots.forEach(slot => {
      slot.selected = false;
    });
    if (!slot.booked) {
      this.selectedSlot = slot.time;
      slot.selected = true;
    }
  }

  onSubmit() {
    this.dialogRef.close(this.selectedSlot);
  }
}
