import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingTimeDialogComponent } from './booking-time-dialog.component';

describe('BookingTimeDialogComponent', () => {
  let component: BookingTimeDialogComponent;
  let fixture: ComponentFixture<BookingTimeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingTimeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingTimeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
