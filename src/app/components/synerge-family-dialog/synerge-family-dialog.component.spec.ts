import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SynergeFamilyDialogComponent } from './synerge-family-dialog.component';

describe('SynergeFamilyDialogComponent', () => {
  let component: SynergeFamilyDialogComponent;
  let fixture: ComponentFixture<SynergeFamilyDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SynergeFamilyDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SynergeFamilyDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
