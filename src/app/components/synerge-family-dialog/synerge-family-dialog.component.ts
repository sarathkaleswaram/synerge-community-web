import { Component, OnInit, Inject } from '@angular/core';
import { SynergeFamily, CompanyHead } from 'app/domains/models/synergeFamily';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { SynergeService } from 'app/services/synerge.service';
import { ApiService } from 'app/services/api.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { LoggerService } from 'app/services/logger.service';
import { ResponseFormat } from 'app/domains/response-format';
import { ImageCropperDialogComponent } from '../image-cropper-dialog/image-cropper-dialog.component';

@Component({
  selector: 'app-synerge-family-dialog',
  templateUrl: './synerge-family-dialog.component.html',
  styleUrls: ['./synerge-family-dialog.component.scss']
})
export class SynergeFamilyDialogComponent implements OnInit {
  synergeFamily: SynergeFamily = {
    belongs_to: undefined,
    company_name: undefined,
    company_description: undefined,
    company_link: undefined,
    company_logo_path: undefined,
    company_heads: []
  };
  synergeFamilyForm: FormGroup;
  buttonText = "Save";
  imagePath;
  imgURL: any;
  message: string;
  companyHeads: CompanyHead[] = [{
    designation: undefined,
    name: undefined
  }];
  dialogHeader: string;

  constructor(public synergeService: SynergeService,
    private apiService: ApiService,
    private formBuilder: FormBuilder,
    private logger: LoggerService,
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<SynergeFamilyDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public synergeFamilyId) { }

  ngOnInit() {
    if (this.synergeFamilyId) {
      this.logger.debug("Edit SynergeFamily ID: " + this.synergeFamilyId);
      this.apiService.getSynergeFamilyById(this.synergeFamilyId).subscribe(data => {
        this.logger.debug("Edit Synerge Family by ID: " + JSON.stringify(data));
        this.dialogHeader = "Edit Synerge Family";
        this.synergeFamily = data.data;
        this.imgURL = this.synergeService.basePath + this.synergeFamily.company_logo_path;
        this.companyHeads = this.synergeFamily.company_heads;
        this.synergeFamilyForm = this.formBuilder.group({
          belongs_to: new FormControl(this.synergeFamily.belongs_to, Validators.required),
          company_name: new FormControl(this.synergeFamily.company_name, Validators.required),
          company_description: new FormControl(this.synergeFamily.company_description),
          company_link: new FormControl(this.synergeFamily.company_link),
        });
      }, error => {
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.logger.debug("Create Synergefamily");
      this.dialogHeader = "Add Synerge Family";
      this.synergeFamilyForm = this.formBuilder.group({
        belongs_to: new FormControl('', Validators.required),
        company_name: new FormControl('', Validators.required),
        company_description: new FormControl(''),
        company_link: new FormControl('')
      });
    }
  }

  preview(event) {
    if (event.target.files.length === 0)
      return;

    let mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    } else {
      this.message = "";
    }
    
    const eventDialog = this.dialog.open(ImageCropperDialogComponent, {
      width: '800px',
      data: event
    });
    eventDialog.afterClosed().subscribe(result => {
      if (result) {
        this.imgURL = result.croppedImage;
        this.imagePath = result.croppedFile;
      }
      const element = event.target as HTMLInputElement;
      element.value = '';
    });

    // let reader = new FileReader();
    // this.imagePath = files[0];
    // reader.readAsDataURL(files[0]);
    // reader.onload = (_event) => {
    //   this.imgURL = reader.result;
    // }
  }

  onSave() {
    if (this.imagePath) {
      this.buttonText = "Loading...";
      let formData: FormData = new FormData();
      formData.append('uploadImage', this.imagePath);
      this.apiService.uploadImage(formData).subscribe((res: ResponseFormat) => {
        if (res.success) {
          this.synergeFamily.company_logo_path = '/uploads/' + res.data;
          this.saveSynergeFamily();
        }
      }, error => {
        this.buttonText = "Save";
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.saveSynergeFamily();
    }
  }

  saveSynergeFamily() {
    if (this.synergeFamilyForm.valid) {
      this.buttonText = "Loading...";
      this.companyHeads = this.companyHeads.filter(x => x.designation && x.name);
      this.synergeFamily.belongs_to = this.synergeFamilyForm.value.belongs_to;
      this.synergeFamily.company_name = this.synergeFamilyForm.value.company_name;
      this.synergeFamily.company_description = this.synergeFamilyForm.value.company_description;
      this.synergeFamily.company_link = this.synergeFamilyForm.value.company_link;
      this.synergeFamily.company_heads = this.companyHeads;
      if (this.synergeFamilyId) {
        this.apiService.updateSynergeFamilyById(this.synergeFamily).subscribe((res: ResponseFormat) => {
          this.logger.debug("Update SynergeFamily res: " + JSON.stringify(res));
          if (res.success) {
            this.buttonText = "Success";
            setTimeout(() => {
              this.dialogRef.close("Success");
            }, 1000);
          } else {
            this.buttonText = "Save";
            this.message = res.message;
          }
        }, error => {
          this.buttonText = "Save";
          this.synergeService.handleErrorOnAPI(error);
        });
      } else {
        this.apiService.createSynergeFamily(this.synergeFamily).subscribe((res: ResponseFormat) => {
          this.logger.debug("Save SynergeFamily res: " + JSON.stringify(res));
          if (res.success) {
            this.buttonText = "Success";
            setTimeout(() => {
              this.dialogRef.close("Success");
            }, 1000);
          } else {
            this.buttonText = "Save";
            this.message = res.message;
          }
        }, error => {
          this.buttonText = "Save";
          this.synergeService.handleErrorOnAPI(error);
        });
      }
    }
  }

  addRow(i: number) {
    let companyHeads: CompanyHead = { designation: undefined, name: undefined };
    if (this.companyHeads.length <= i + 1) {
      this.companyHeads.splice(i + 1, 0, companyHeads);
    }
  }

  deleteRow($event, companyHeads: CompanyHead, i) {
    let index = this.companyHeads.indexOf(companyHeads);
    if ($event.which == 1) {
      this.companyHeads.splice(index, 1);
    }
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.synergeFamilyForm.controls[controlName].hasError(errorName);
  }

}
