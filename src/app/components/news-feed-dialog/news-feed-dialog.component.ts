import { Component, OnInit } from '@angular/core';
import { NewsFeed } from 'app/domains/models/newsFeed';
import { ResponseFormat } from 'app/domains/response-format';
import { SynergeService } from 'app/services/synerge.service';
import { ApiService } from 'app/services/api.service';
import { AppConstants } from 'app/domains/app-constants';
import { LoggerService } from 'app/services/logger.service';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Carousel } from 'app/domains/models/carousel';
import { ImageCropperDialogComponent } from '../image-cropper-dialog/image-cropper-dialog.component';
import { ImageRatioCropperDialogComponent } from '../image-ratio-cropper-dialog/image-ratio-cropper-dialog.component';

@Component({
  selector: 'app-news-feed-dialog',
  templateUrl: './news-feed-dialog.component.html',
  styleUrls: ['./news-feed-dialog.component.scss']
})
export class NewsFeedDialogComponent implements OnInit {
  newsFeed: NewsFeed = {
    by_user_id: undefined,
    by_user_name: undefined,
    date: undefined,
    content: undefined,
    image_path: undefined,
    likes: [],
    comments: []
  };
  carousel: Carousel = {
    by_user_id: undefined,
    by_user_name: undefined,
    date: undefined,
    image_path: undefined,
  };
  content: string = "";
  buttonText = "Save";
  imagePath;
  imgURL: any;
  message: string;

  type = 'NewsFeed';
  isAdmin = localStorage.getItem(AppConstants.USER_ROLE) === 'ADMIN' ? true : false;

  constructor(
    public synergeService: SynergeService,
    private apiService: ApiService,
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<NewsFeedDialogComponent>,
    private logger: LoggerService) { }

  ngOnInit() {
  }

  preview(event) {
    if (event.target.files.length === 0)
      return;

    let mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    } else {
      this.message = "";
    }

    if (this.type === 'NewsFeed') {
      let reader = new FileReader();
      this.imagePath = event.target.files[0];
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (_event) => {
        this.imgURL = reader.result;
      }
    } else {
      const eventDialog = this.dialog.open(ImageRatioCropperDialogComponent, {
        width: '800px',
        data: { aspectRatio: 16 / 9, imageEvent: event }
      });
      eventDialog.afterClosed().subscribe(result => {
        if (result) {
          this.imgURL = result.croppedImage;
          this.imagePath = result.croppedFile;
        }
        const element = event.target as HTMLInputElement;
        element.value = '';
      });
    }
  }

  onSave() {
    if (this.type === 'NewsFeed') {
      this.saveNewsFeed();
    } else {
      this.saveCarousel();
    }
  }

  saveCarousel() {
    if (this.imagePath) {
      this.buttonText = "Loading...";
      let formData: FormData = new FormData();
      formData.append('uploadImage', this.imagePath);
      this.apiService.uploadImage(formData).subscribe((res: ResponseFormat) => {
        if (res.success) {
          this.carousel.image_path = '/uploads/' + res.data;
          this.carousel.date = new Date();
          this.carousel.by_user_id = localStorage.getItem(AppConstants.USER_ID);
          this.carousel.by_user_name = localStorage.getItem(AppConstants.USER_FULL_NAME);
          this.apiService.saveCarousel(this.carousel).subscribe((res: ResponseFormat) => {
            this.logger.debug("Carousel: " + JSON.stringify(res));
            if (res.success) {
              this.buttonText = "Success";
              setTimeout(() => {
                this.dialogRef.close("Success");
              }, 1000);
            } else {
              this.buttonText = "Save";
              this.message = res.message;
            }
          }, error => {
            this.buttonText = "Save";
            this.synergeService.handleErrorOnAPI(error);
          });
        }
      }, error => {
        this.buttonText = "Save";
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.message = "Please upload image.";
    }
  }

  saveNewsFeed() {
    if (this.imagePath) {
      this.buttonText = "Loading...";
      let formData: FormData = new FormData();
      formData.append('uploadImage', this.imagePath);
      this.apiService.uploadImage(formData).subscribe((res: ResponseFormat) => {
        if (res.success) {
          this.newsFeed.image_path = '/uploads/' + res.data;
          this.saveContent();
        }
      }, error => {
        this.buttonText = "Save";
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.saveContent();
    }
  }

  saveContent() {
    if (this.content.length > 0) {
      this.buttonText = "Loading...";
      this.newsFeed.content = this.content;
      this.newsFeed.date = new Date();
      this.newsFeed.by_user_id = localStorage.getItem(AppConstants.USER_ID);
      this.newsFeed.by_user_name = localStorage.getItem(AppConstants.USER_FULL_NAME);
      this.apiService.saveNewsFeed(this.newsFeed).subscribe((res: ResponseFormat) => {
        this.logger.debug("News Feed: " + JSON.stringify(res));
        if (res.success) {
          this.buttonText = "Success";
          setTimeout(() => {
            this.dialogRef.close("Success");
          }, 1000);
        } else {
          this.buttonText = "Save";
          this.message = res.message;
        }
      }, error => {
        this.buttonText = "Save";
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.buttonText = "Save";
      this.message = "Please enter content.";
    }
  }

}
