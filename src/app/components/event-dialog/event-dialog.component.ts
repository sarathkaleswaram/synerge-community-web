import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Event } from 'app/domains/models/events';
import { SynergeService } from 'app/services/synerge.service';
import { ApiService } from 'app/services/api.service';
import { LoggerService } from 'app/services/logger.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ResponseFormat } from 'app/domains/response-format';
import { ImageCropperDialogComponent } from '../image-cropper-dialog/image-cropper-dialog.component';

@Component({
  selector: 'app-event-dialog',
  templateUrl: './event-dialog.component.html',
  styleUrls: ['./event-dialog.component.scss']
})
export class EventDialogComponent implements OnInit {
  eventForm: FormGroup;
  event: Event = {
    date: undefined,
    belongs_to: undefined,
    type: undefined,
    title: undefined,
    description: undefined,
    pics: [],
    links: [],
    likes: [],
    comments: []
  };
  imagePath;
  imgURL: any;
  message: string;
  buttonText = "Save";
  dialogHeader: string;

  constructor(
    private formBuilder: FormBuilder,
    private synergeService: SynergeService,
    private apiService: ApiService,
    private logger: LoggerService,
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<EventDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public eventId) { }

  ngOnInit() {
    if (this.eventId) {
      this.logger.debug("Edit Event ID: " + this.eventId);
      this.apiService.getEventById(this.eventId).subscribe(data => {
        this.logger.debug("Edit Event - Event by ID: " + JSON.stringify(data));
        this.dialogHeader = "Edit Event";
        this.event = data.data;
        this.imgURL = this.synergeService.basePath + this.event.pics[0];
        this.eventForm = this.formBuilder.group({
          type: new FormControl(this.event.type, Validators.required),
          belongs_to: new FormControl(this.event.belongs_to, Validators.required),
          title: new FormControl(this.event.title, Validators.required),
          description: new FormControl(this.event.description),
          date: new FormControl(this.event.date, Validators.required),
          links: new FormControl(this.event.links[0])
        });
      }, error => {
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.logger.debug("Create Event");
      this.dialogHeader = "Add Event";
      this.eventForm = this.formBuilder.group({
        type: new FormControl('', Validators.required),
        belongs_to: new FormControl('', Validators.required),
        title: new FormControl('', Validators.required),
        description: new FormControl(''),
        date: new FormControl('', Validators.required),
        links: new FormControl(''),
      });
    }
  }

  preview(event) {
    if (event.target.files.length === 0)
      return;

    let mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    } else {
      this.message = "";
    }
    
    const eventDialog = this.dialog.open(ImageCropperDialogComponent, {
      width: '800px',
      data: event
    });
    eventDialog.afterClosed().subscribe(result => {
      if (result) {
        this.imgURL = result.croppedImage;
        this.imagePath = result.croppedFile;
      }
      const element = event.target as HTMLInputElement;
      element.value = '';
    });
  }

  onSave() {
    if (this.imagePath) {
      this.buttonText = "Loading...";
      let formData: FormData = new FormData();
      formData.append('uploadImage', this.imagePath);
      this.apiService.uploadImage(formData).subscribe((res: ResponseFormat) => {
        if (res.success) {
          this.event.pics[0] = '/uploads/' + res.data;
          this.saveEvent();
        }
      }, error => {
        this.buttonText = "Save";
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.saveEvent();
    }
  }

  saveEvent() {
    if (this.eventForm.valid) {
      this.buttonText = "Loading...";
      this.event.date = this.eventForm.value.date;
      this.event.belongs_to = this.eventForm.value.belongs_to;
      this.event.type = this.eventForm.value.type;
      this.event.title = this.eventForm.value.title;
      this.event.description = this.eventForm.value.description;
      this.event.links[0] = this.eventForm.value.links;
      if (this.eventId) {
        this.apiService.updateEventById(this.event).subscribe((res: ResponseFormat) => {
          this.logger.debug("Event Update: " + JSON.stringify(res));
          if (res.success) {
            this.buttonText = "Success";
            setTimeout(() => {
              this.dialogRef.close("Success");
            }, 1000);
          } else {
            this.buttonText = "Save";
            this.synergeService.handleError(res.message);
          }
        }, error => {
          this.buttonText = "Save";
          this.synergeService.handleErrorOnAPI(error);
        });
      } else {
        this.apiService.createEvent(this.event).subscribe((res: ResponseFormat) => {
          this.logger.debug("Event res: " + JSON.stringify(res));
          if (res.success) {
            this.buttonText = "Success";
            setTimeout(() => {
              this.dialogRef.close("Success");
            }, 1000);
          } else {
            this.buttonText = "Save";
            this.message = res.message;
          }
        }, error => {
          this.buttonText = "Save";
          this.synergeService.handleErrorOnAPI(error);
        });
      }
    }
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.eventForm.controls[controlName].hasError(errorName);
  }

}
