import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddClientVisitorsDialogComponent } from './add-client-visitors-dialog.component';

describe('AddClientVisitorsDialogComponent', () => {
  let component: AddClientVisitorsDialogComponent;
  let fixture: ComponentFixture<AddClientVisitorsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddClientVisitorsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddClientVisitorsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
