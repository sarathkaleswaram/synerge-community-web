import { Component, OnInit } from '@angular/core';
import { Feedback } from 'app/domains/models/feedback';
import { SynergeService } from 'app/services/synerge.service';
import { ApiService } from 'app/services/api.service';
import { LoggerService } from 'app/services/logger.service';
import { MatDialogRef } from '@angular/material';
import { ResponseFormat } from 'app/domains/response-format';
import { AppConstants } from 'app/domains/app-constants';

@Component({
  selector: 'app-add-feedback-dialog',
  templateUrl: './add-feedback-dialog.component.html',
  styleUrls: ['./add-feedback-dialog.component.scss']
})
export class AddFeedbackDialogComponent implements OnInit {

  buttonText = "Submit";
  message: string = "";
  feedback: string = "";

  constructor(public synergeService: SynergeService,
    private apiService: ApiService,
    private logger: LoggerService,
    public dialogRef: MatDialogRef<AddFeedbackDialogComponent>) { }

  ngOnInit() {
  }

  onSubmit() {
    this.message = "";
    if (this.feedback == "") {
      this.message = "Please enter all fields.";
      return;
    }
    let belongs_to: any = localStorage.getItem(AppConstants.USER_BELONGS_TO);
    let feedback: Feedback = {
      by_user_id: localStorage.getItem(AppConstants.USER_ID),
      by_user_name: localStorage.getItem(AppConstants.USER_FULL_NAME),
      feedback: this.feedback,
      requested_date: new Date(),
      belongs_to: belongs_to,
      company_name: localStorage.getItem(AppConstants.USER_COMPANY_NAME),
      comments: [],
      status: "Not Seen"
    };
    this.apiService.createFeedback(feedback).subscribe((res: ResponseFormat) => {
      this.logger.debug("Feedback Saved data: " + JSON.stringify(res));
      if (res.success) {
        this.buttonText = "Success";
        setTimeout(() => {
          this.dialogRef.close("Success");
        }, 1000);
      } else {
        this.buttonText = "Submit";
        this.synergeService.handleError(res.message);
      }
    }, error => {
      this.buttonText = "Submit";
      this.synergeService.handleErrorOnAPI(error);
    });
  }

}
