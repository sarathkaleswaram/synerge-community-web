import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AchieversDialogComponent } from './achievers-dialog.component';

describe('AchieversDialogComponent', () => {
  let component: AchieversDialogComponent;
  let fixture: ComponentFixture<AchieversDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AchieversDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AchieversDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
