import { Component, OnInit, Inject } from '@angular/core';
import { Achiever } from 'app/domains/models/achievers';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { SynergeService } from 'app/services/synerge.service';
import { ApiService } from 'app/services/api.service';
import { LoggerService } from 'app/services/logger.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { ResponseFormat } from 'app/domains/response-format';
import { Clients } from 'app/domains/models/clients';
import { ImageCropperDialogComponent } from '../image-cropper-dialog/image-cropper-dialog.component';

@Component({
  selector: 'app-achievers-dialog',
  templateUrl: './achievers-dialog.component.html',
  styleUrls: ['./achievers-dialog.component.scss']
})
export class AchieversDialogComponent implements OnInit {
  achiever: Achiever = {
    belongs_to: undefined,
    company_name: undefined,
    achiever: undefined,
    achieved: undefined,
    pic: undefined,
    date: undefined
  };
  clients: Clients[] = [];
  companyNames = [];
  achieverForm: FormGroup;
  buttonText = "Save";
  imagePath;
  imgURL: any;
  message: string;
  dialogHeader: string;

  constructor(public synergeService: SynergeService,
    private apiService: ApiService,
    private formBuilder: FormBuilder,
    private logger: LoggerService,
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<AchieversDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public achieverId) { }

  ngOnInit() {
    this.apiService.getAllActiveClients().subscribe((res: ResponseFormat) => {
      this.logger.debug("Got clients count: " + res.data.length);
      this.clients = res.data;
    }, error => {
      this.synergeService.handleErrorOnAPI(error);
    });
    if (this.achieverId) {
      this.logger.debug("Edit Achiever ID: " + this.achieverId);
      this.apiService.getAchieverById(this.achieverId).subscribe(data => {
        this.dialogHeader = "Edit Achiever"
        this.logger.debug("Edit Achiever by ID: " + JSON.stringify(data));
        this.achiever = data.data;
        this.onBelongsToChange(this.achiever.belongs_to);
        this.imgURL = this.synergeService.basePath + this.achiever.pic;
        this.achieverForm = this.formBuilder.group({
          belongs_to: new FormControl(this.achiever.belongs_to, Validators.required),
          company_name: new FormControl(this.achiever.company_name, Validators.required),
          achiever: new FormControl(this.achiever.achiever, Validators.required),
          achieved: new FormControl(this.achiever.achieved, Validators.required),
          date: new FormControl(this.achiever.date, Validators.required),
        });
      }, error => {
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.logger.debug("Create Achiever");
      this.dialogHeader = "Add Achiever"
      this.achieverForm = this.formBuilder.group({
        belongs_to: new FormControl('', Validators.required),
        company_name: new FormControl('', Validators.required),
        achiever: new FormControl('', Validators.required),
        achieved: new FormControl('', Validators.required),
        date: new FormControl('', Validators.required)
      });
    }
  }

  onBelongsToChange(belongsTo) {
    this.logger.debug("onBelongsToChange: " + belongsTo);
    if (belongsTo === 'SYNERGE_I') {
      this.companyNames = this.clients.filter(x => x.belongs_to === 'SYNERGE_I');
    } else {
      this.companyNames = this.clients.filter(x => x.belongs_to === 'SYNERGE_II');
    }
  }

  preview(event) {
    if (event.target.files.length === 0)
      return;

    let mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    } else {
      this.message = "";
    }

    const eventDialog = this.dialog.open(ImageCropperDialogComponent, {
      width: '800px',
      data: event
    });
    eventDialog.afterClosed().subscribe(result => {
      if (result) {
        this.imgURL = result.croppedImage;
        this.imagePath = result.croppedFile;
      }
      const element = event.target as HTMLInputElement;
      element.value = '';
    });
  }

  onSave() {
    if (this.imagePath) {
      this.buttonText = "Loading...";
      let formData: FormData = new FormData();
      formData.append('uploadImage', this.imagePath);
      this.apiService.uploadImage(formData).subscribe((res: ResponseFormat) => {
        if (res.success) {
          this.achiever.pic = '/uploads/' + res.data;
          this.saveAchiever();
        }
      }, error => {
        this.buttonText = "Save";
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.saveAchiever();
    }
  }

  saveAchiever() {
    if (this.achieverForm.valid) {
      this.buttonText = "Loading...";
      this.achiever.belongs_to = this.achieverForm.value.belongs_to;
      this.achiever.company_name = this.achieverForm.value.company_name;
      this.achiever.achieved = this.achieverForm.value.achieved;
      this.achiever.achiever = this.achieverForm.value.achiever;
      this.achiever.date = this.achieverForm.value.date;
      if (this.achieverId) {
        this.apiService.updateAchieverById(this.achiever).subscribe((res: ResponseFormat) => {
          this.logger.debug("Update Achiever res: " + JSON.stringify(res));
          if (res.success) {
            this.buttonText = "Success";
            setTimeout(() => {
              this.dialogRef.close("Success");
            }, 1000);
          } else {
            this.buttonText = "Save";
            this.message = res.message;
          }
        }, error => {
          this.buttonText = "Save";
          this.synergeService.handleErrorOnAPI(error);
        });
      } else {
        this.apiService.createAchiever(this.achiever).subscribe((res: ResponseFormat) => {
          this.logger.debug("Save Achiever res: " + JSON.stringify(res));
          if (res.success) {
            this.buttonText = "Success";
            setTimeout(() => {
              this.dialogRef.close("Success");
            }, 1000);
          } else {
            this.buttonText = "Save";
            this.message = res.message;
          }
        }, error => {
          this.buttonText = "Save";
          this.synergeService.handleErrorOnAPI(error);
        });
      }
    }
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.achieverForm.controls[controlName].hasError(errorName);
  }

}
