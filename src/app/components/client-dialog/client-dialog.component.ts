import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { SynergeService } from 'app/services/synerge.service';
import { ApiService } from 'app/services/api.service';
import { LoggerService } from 'app/services/logger.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Clients } from 'app/domains/models/clients';
import { ResponseFormat } from 'app/domains/response-format';

@Component({
  selector: 'app-client-dialog',
  templateUrl: './client-dialog.component.html',
  styleUrls: ['./client-dialog.component.scss']
})
export class ClientDialogComponent implements OnInit {
  clientForm: FormGroup;
  client: Clients = {
    active_status: undefined,
    belongs_to: undefined,
    company_name: undefined,
    joined_date: undefined
  };
  buttonText = "Confirm";
  dialogHeader: string;

  constructor(
    private formBuilder: FormBuilder,
    private synergeService: SynergeService,
    private apiService: ApiService,
    private logger: LoggerService,
    public dialogRef: MatDialogRef<ClientDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public clientId) { }

  ngOnInit() {
    if (this.clientId) {
      this.logger.debug("Edit Client ID: " + this.clientId);
      this.apiService.getClientById(this.clientId).subscribe(data => {
        this.logger.debug("Edit Client - Client by ID: " + JSON.stringify(data));
        this.dialogHeader = "Edit Client";
        this.client = data.data;
        this.clientForm = this.formBuilder.group({
          company_name: new FormControl(this.client.company_name, Validators.required),
          belongs_to: new FormControl(this.client.belongs_to, Validators.required),
          active_status: new FormControl(this.client.active_status, Validators.required),
          joined_date: new FormControl(this.client.joined_date, Validators.required),
          leaving_date: new FormControl(this.client.leaving_date)
        });
      }, error => {
        this.synergeService.handleErrorOnAPI(error);
      });
    } else {
      this.logger.debug("Create Client");
      this.dialogHeader = "Add Client";
      this.clientForm = this.formBuilder.group({
        company_name: new FormControl('', Validators.required),
        belongs_to: new FormControl('', Validators.required),
        active_status: new FormControl('', Validators.required),
        joined_date: new FormControl('', Validators.required),
        leaving_date: new FormControl('')
      });
    }
  }

  onSubmit() {
    if (this.clientForm.valid) {
      this.buttonText = "Loading...";
      this.client.company_name = this.clientForm.value.company_name;
      this.client.belongs_to = this.clientForm.value.belongs_to;
      this.client.active_status = this.clientForm.value.active_status;
      this.client.joined_date = this.clientForm.value.joined_date;
      this.client.leaving_date = this.clientForm.value.leaving_date;
      if (this.client.active_status === "Active")
        this.client.leaving_date = null;
      if (this.clientId) {
        this.apiService.updateClientById(this.client).subscribe((res: ResponseFormat) => {
          this.logger.debug("Client Update: " + JSON.stringify(res));
          if (res.success) {
            this.buttonText = "Success";
            setTimeout(() => {
              this.dialogRef.close(res.data);
            }, 1000);
          } else {
            this.buttonText = "Confirm";
            this.synergeService.handleError(res.message);
          }
        }, error => {
          this.buttonText = "Confirm";
          this.synergeService.handleErrorOnAPI(error);
        });
      } else {
        this.apiService.createClient(this.client).subscribe((res: ResponseFormat) => {
          this.logger.debug("Client Create: " + JSON.stringify(res));
          if (res.success) {
            this.buttonText = "Success";
            setTimeout(() => {
              this.dialogRef.close(res.data);
            }, 1000);
          } else {
            this.buttonText = "Confirm";
            this.synergeService.handleError(res.message);
          }
        }, error => {
          this.buttonText = "Confirm";
          this.synergeService.handleErrorOnAPI(error);
        });
      }
    }
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.clientForm.controls[controlName].hasError(errorName);
  }

}
