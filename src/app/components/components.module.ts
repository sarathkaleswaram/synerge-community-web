import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ImageCropperModule } from 'ngx-image-cropper';
import { MatDialogModule, MatFormFieldModule, MatInputModule, MatCheckboxModule, MatCardModule, MatDatepickerModule, MatNativeDateModule, MatSelectModule, MatRadioModule, MatSlideToggleModule } from '@angular/material';

import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ChangePasswordDialogComponent } from './change-password-dialog/change-password-dialog.component';
import { NewsFeedDialogComponent } from './news-feed-dialog/news-feed-dialog.component';
import { AddBookingDialogComponent } from './add-booking-dialog/add-booking-dialog.component';
import { BookingTimeDialogComponent } from './booking-time-dialog/booking-time-dialog.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { AlertDialogComponent } from './alert-dialog/alert-dialog.component';
import { AddClientVisitorsDialogComponent } from './add-client-visitors-dialog/add-client-visitors-dialog.component';
import { ClientDialogComponent } from './client-dialog/client-dialog.component';
import { AddFeedbackDialogComponent } from './add-feedback-dialog/add-feedback-dialog.component';
import { EventDialogComponent } from './event-dialog/event-dialog.component';
import { EditFeedbackDialogComponent } from './edit-feedback-dialog/edit-feedback-dialog.component';
import { SynergeFamilyDialogComponent } from './synerge-family-dialog/synerge-family-dialog.component';
import { AchieversDialogComponent } from './achievers-dialog/achievers-dialog.component';
import { ImageCropperDialogComponent } from './image-cropper-dialog/image-cropper-dialog.component';
import { ImageRatioCropperDialogComponent } from './image-ratio-cropper-dialog/image-ratio-cropper-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    ImageCropperModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDialogModule,
    MatCheckboxModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSlideToggleModule
  ],
  declarations: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    ChangePasswordDialogComponent,
    NewsFeedDialogComponent,
    AddBookingDialogComponent,
    BookingTimeDialogComponent,
    ConfirmationDialogComponent,
    AlertDialogComponent,
    ClientDialogComponent,
    AddClientVisitorsDialogComponent,
    EventDialogComponent,
    AddFeedbackDialogComponent,
    EditFeedbackDialogComponent,
    SynergeFamilyDialogComponent,
    AchieversDialogComponent,
    ImageCropperDialogComponent,
    ImageRatioCropperDialogComponent
  ],
  exports: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    ChangePasswordDialogComponent,
    NewsFeedDialogComponent,
    AddBookingDialogComponent,
    BookingTimeDialogComponent,
    ConfirmationDialogComponent,
    AlertDialogComponent,
    ClientDialogComponent,
    AddClientVisitorsDialogComponent,
    EventDialogComponent,
    AddFeedbackDialogComponent,
    EditFeedbackDialogComponent,
    SynergeFamilyDialogComponent,
    AchieversDialogComponent,
    ImageCropperDialogComponent,
    ImageRatioCropperDialogComponent
  ],
  entryComponents: [
    ChangePasswordDialogComponent,
    NewsFeedDialogComponent,
    AddBookingDialogComponent,
    BookingTimeDialogComponent,
    ConfirmationDialogComponent,
    AlertDialogComponent,
    ClientDialogComponent,
    AddClientVisitorsDialogComponent,
    EventDialogComponent,
    AddFeedbackDialogComponent,
    EditFeedbackDialogComponent,
    SynergeFamilyDialogComponent,
    AchieversDialogComponent,
    ImageCropperDialogComponent,
    ImageRatioCropperDialogComponent
  ]
})
export class ComponentsModule { }
