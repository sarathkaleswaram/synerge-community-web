import { Component } from '@angular/core';
import { SynergeService } from './services/synerge.service';
import { AppConstants } from './domains/app-constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(public synergeService: SynergeService) {
    if (localStorage.getItem(AppConstants.DARK_EDITION_ACTIVE)) {
      this.synergeService.darkEdition = localStorage.getItem(AppConstants.DARK_EDITION_ACTIVE) === 'true';
    } else {
      this.synergeService.darkEdition = true;
    }
    this.synergeService.onThemeChange();
  }

}
