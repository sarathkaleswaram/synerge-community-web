export type BelongsToEnum = 'SYNERGE_I' | 'SYNERGE_II';

export interface Achiever {
    _id?: string;
    achiever: string;
    achieved: string;
    belongs_to: BelongsToEnum;
    company_name: string;
    date: Date;
    pic: string;
};