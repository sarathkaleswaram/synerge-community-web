type RoleEnum = 'ADMIN' | 'CLIENT';
type BelongsToEnum = 'SYNERGE_I' | 'SYNERGE_II';

export interface User {
    _id?: string;
    first_name: string;
    last_name: string;
    user_name: string;
    password: string;
    email: string;
    phone: number;
    address?: string;
    city?: string;
    country?: string;
    postal_code?: string;
    role: RoleEnum;
    belongs_to?: BelongsToEnum;
    company_name?: string;
    activated: boolean;
    deleted: boolean;
    created_on: Date;
    updated_on: Date;
    profile_pic_path?: string;
    designation?: string;
    bio?: string;
};