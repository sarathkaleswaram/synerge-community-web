export type StatusEnum = 'Request Pending' | 'Confirmed' | 'Declined' | 'Completed';

export interface Booking {
    _id?: string;
    by_user_id: string;
    by_user_name: string;
    booking_created_date: Date;
    belongs_to: string;
    company_name: string;   
    booking_type: string;
    booking_date: Date;
    booking_time: string;
    status: StatusEnum;
    booking_note?: string;
};