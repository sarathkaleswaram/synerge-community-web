import { Injectable } from '@angular/core';
import { Router, CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { AppConstants } from './app-constants';
import { MatDialog } from '@angular/material';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(public router: Router, private dialogRef: MatDialog) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    this.dialogRef.closeAll();
    if (!localStorage.getItem(AppConstants.TOKEN)) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}